<?php

include 'Calculate.php';
include 'Validate.php';
include 'AlterRequest.php';

$request = new AlterRequest($_GET);
$values = $request -> returnValues();
try {
	
	(new Validate()) -> validateInput(array($values['numbers'], $values['sighns']));


	$calc = new Calculate($values['numbers'], $values['sighns']);
	
	$result = $calc -> operate(); 
	echo $result;



}
catch(SynException $e) {
	echo $e -> getMessage();
}
catch(MaException $e) {
	echo $e -> getMessage();
}



