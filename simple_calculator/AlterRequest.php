<?php



class AlterRequest
{

	protected $request;

	public function __construct($req) {
		$this -> request = $req;
	}

	public function returnValues() {
		
		$values = $this -> getValues();
		return $values;
	}

	

	protected function getValues() {
		$return = array();
		foreach($this -> request as $key => $value) {
			if($this -> isJson($value)) {
				$value = $this -> jsonDecode($value);
			}		
			$return[$key] = $value;
		}
		return $return;
	}


	protected function jsonDecode($val) {
		return json_decode($val);
	}
	
	protected function isJson($str) {
		$string = json_decode($str);
		return (json_last_error() == JSON_ERROR_NONE);
	}
}