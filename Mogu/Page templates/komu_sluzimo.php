<?php
/*
 * Template Name: Predlozak Komu Sluzimo
 */
?>
<?php get_header(); ?>
<div class ="container">
    <div class="curvedBorder">
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
             <h2><b><div style ="color:<?php the_field( 'page_komu_sluzimo_title_color'); ?>" ><?php the_field( 'page_komu_sluzimo_title' ); ?></div></b></h2>
        </div>
    </div>
    </br>
    <div class="row">
        <div class ="col-md-5 col-lg-5 col-sm-5 col-xs-5">
            <?php
                if( have_rows( 'page_komu_sluzimo_repeater_images') ) :
                    while( have_rows( 'page_komu_sluzimo_repeater_images' ) ) :
                        the_row(); ?>
                        <?php $image = get_sub_field( 'page_komu_sluzimo_repeater_images_image' ); ?>
                        <a href="<?php echo $image['url']; ?>" target ="blank" ><img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['title']; ?>" /></a>
                    <?php endwhile;
                endif;
            ?>    
        </div>
        <div class ="col-md-7 col-lg-7 col-sm-7 col-xs-7 col-md-pull-1 col-lg-pull-1 col-sm-pull-1">
        <?php if( get_field( 'page_komu_sluzimo_frame_check') ) : ?>
                
                <?php if( in_array( 'Sa okvirom', get_field( 'page_komu_sluzimo_frame_check') ) ) : ?>
                    <table cellpadding="50" align="center" style ="border:5px solid black"><tr><td align="center">
                <?php endif;
            endif; ?>
            <?php the_field( 'page_komu_sluzimo_text' ); ?>
            <?php if( get_field( 'page_komu_sluzimo_frame_check') ) : ?>
                <?php if( in_array( 'Sa okvirom', get_field( 'page_komu_sluzimo_frame_check') ) ) : ?>
                    </td></tr></table>
                
                <?php endif;
            endif; ?>
        </div>
    </div>
    </br>
    </div>
</div>
<?php get_footer(); ?>

