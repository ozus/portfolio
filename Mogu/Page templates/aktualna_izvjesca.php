<?php
/*
 * Template Name: Predlozak Aktualna Izvjesca
 */
?>
<?php get_header(); ?>
<div class ="container">
    <div class="curvedBorder">
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
             <h2><b><div style ="color:<?php the_field( 'page_izvjesca_color'); ?>" ><?php the_field( 'page_izvjesca_title' ); ?></div></b></h2>
        </div>
    </div>
    </br>
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
            <?php if( get_field( 'page_izvjesca_framecheck') ) : ?>
                </br>
                </br>
                <?php if( in_array( 'Sa okvirom', get_field( 'page_izvjesca_framecheck') ) ) : ?>
                    <table cellpadding="50" align="center" style ="border:5px solid black"><tr><td align="center">
                <?php endif;
            endif; ?>
            <?php the_field( 'page_izvjesca_text' ); ?>
            <?php if( get_field( 'page_izvjesca_framecheck') ) : ?>
                <?php if( in_array( 'Sa okvirom', get_field( 'page_izvjesca_framecheck') ) ) : ?>
                    </td></tr></table>
                <?php endif;
            endif; ?>
        </div>
    </div>
    </br>
   <div class ="row">
       <div class ="panel-group" id="accordion">
           <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
               <div class="panel panel-default">
                   <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            <center><?php the_field( 'page_izvjesca_title_reports' ); ?></center>
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <?php
                                query_posts( array(
                                    'post_type' => 'post',
                                    'category_name' => 'izvjesca',
                                    'posts_per_page' => 20
                                ));
                                if( have_posts() ) : while( have_posts() ) : the_post();
                            ?>


                                            <table border="0" bgcolor="#E0E0E0" style="width:430px"><tr><td bgcolor="#E0E0E0">
                                            <h3><b><div style="color:#000000; text-decoration:none"><a href ="<?php echo get_permalink(); ?>" target="blank"><?php the_title(); ?></a></div></b></h3>
                                            </td></tr></table>



                                        <table border="0" bgcolor="#000000" style="border-bottom:3px black solid; width:430px;">
                                            <tr>
                                                <td align="left" bgcolor="#E0E0E0" style="padding:5px">
                                                     <?php echo get_the_date(); ?>
                                                </td>
                                                <td align="left" bgcolor="#E0E0E0" style="padding:5px">
                                                     <?php the_author(); ?>
                                                </td>
                                                <td align="right" bgcolor="#E0E0E0" style="padding:5px">
                                                     <?php comments_popup_link( 'nema komentara', '1 komentar', 'pogledaj komentare' ); ?>
                                                </td>
                                            </tr>
                                        </table>    


                                        <table border="0" style="width:430px" bgcolor="#000000">
                                            <tr>
                                                <td bgcolor="#E0E0E0" style="padding:10px">
                                                    <?php
                                                    global $more;
                                                    $more = 0;
                                                    the_content( 'Pročitajte više >>>');
                                                    ?>
                                                </td>
                                            </tr>
                                        </table>

                                    </br>
                                    </br>
                                <?php endwhile; ?>
                                    <a href="<?php echo get_permalink(get_page_id( 'Arhiva' ) ); ?>">Pogledaj sva izvješća</a>    
                               <?php  endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>    
               </div>      
            </div> 
            <div class ="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            <center><?php the_field( 'page_izvjesca_title_annonciations' ); ?></center>
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <?php
                             query_posts( array(
                                 'post_type' => 'post',
                                 'category_name' => 'obavijesti',
                                 'posts_per_page' => 20
                             ));
                             if( have_posts() ) : while( have_posts() ) : the_post();
                             ?>


                                         <table border="0" bgcolor="#E0E0E0" style="width:430px"><tr><td bgcolor="#E0E0E0">
                                         <h3><b><div style="color:#000000; text-decoration:none"><a href ="<?php echo get_permalink(); ?>" target="blank"><?php the_title(); ?></a></div></b></h3>
                                         </td></tr></table>



                                     <table border="0" bgcolor="#000000" style="border-bottom:3px black solid; width:430px;">
                                         <tr>
                                             <td align="left" bgcolor="#E0E0E0" style="padding:5px">
                                                  <?php echo get_the_date(); ?>
                                             </td>
                                             <td align="left" bgcolor="#E0E0E0" style="padding:5px">
                                                  <?php the_author(); ?>
                                             </td>
                                             <td align="right" bgcolor="#E0E0E0" style="padding:5px">
                                                  <?php comments_popup_link( 'nema komentara', '1 komentar', 'pogledaj komentare' ); ?>
                                             </td>
                                         </tr>
                                     </table>    


                                     <table border="0" style="width:430px" bgcolor="#000000">
                                         <tr>
                                             <td bgcolor="#E0E0E0" style="padding:10px">
                                                 <?php
                                                 global $more1;
                                                 $more1 = 0;
                                                 the_content( 'Pročitajte više >>>');
                                                 ?>
                                             </td>
                                         </tr>
                                     </table>

                                 </br>
                                 </br>
                             <?php endwhile; ?>
                              <a href="<?php echo get_permalink(get_page_id( 'Arhiva Obavijesti' ) ); ?>">Pogledaj sve obavijesti</a> 
                             <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>    
                </div>     
            </div>
       </div>    
    </div>
    </br>
    </div>
</div>
<?php get_footer(); ?>

