<?php
/*
 * Template Name: Predlozak Konj12
 */
?>
<?php get_header(); ?>
<div class ="container">
    <div class="curvedBorder">
    <div class ="row">
        <div class ="col-md-10 col-lg-10 col-sm-10">
             <h2><b><div style ="color:<?php the_field( 'subpage_konj12_title_color'); ?>" ><?php the_field( 'subpage_konj12_title' ); ?></div></b></h2>
        </div>
    </div>
    </br>
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
            <?php if( get_field( 'subpage_konj12_frame_check') ) :
                if( in_array( 'Sa okvirom', get_field( 'subpage_konj12_frame_check') ) ) : ?>
                    <table cellpadding="50" align="center" style ="border:5px solid black"><tr><td align="center">
                <?php endif;
            endif; ?>
            <?php the_field( 'subpage_konje12_text' ); ?>
            <?php if( get_field( 'subpage_konj12_frame_check') ) :
                if( in_array( 'Sa okvirom', get_field( 'subpage_konj12_frame_check') ) ) : ?>
                    </td></tr></table>
                <?php endif;
            endif; ?>
        </div>
    </div>
    <br>
    <div class ="row">
        <div class ="col-md-12 col-lg-12 col-xs-6">
            <center><b><h3>* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *</h3></b></center>
        </div>
    </div>
    </br>
    <div class ="row">
    <div class="col-md-12 col-sm-12 col-xs-12" id="slider-thumbs">
        <ul class="list-inline">
            <?php $counter = 0; ?>
            <?php if( have_rows( 'subpage_konj12_carousel_repeater') ) :
                while( have_rows( 'subpage_konj12_carousel_repeater') ) :
                    the_row(); ?>
                    <?php if( $counter == 0) : ?>
                        <li> <a id="carousel-selector-<?php echo $counter; ?>" class="selected">
                    <?php endif; ?>
                    <?php if( $counter > 0) : ?>
                        <li> <a id="carousel-selector-<?php echo $counter; ?>">
                    <?php endif; ?>
                    <?php if( $counter >= 10) : ?>
                        <li> <a id="carousel-selector2-<?php echo $counter; ?>">
                    <?php endif; ?>   
                    <?php if( $counter >= 20) : ?>
                        <li> <a id="carousel-selector3-<?php echo $counter; ?>">
                    <?php endif; ?>
                    <?php if( $counter >= 30) : ?>
                        <li> <a id="carousel-selector4-<?php echo $counter; ?>">
                    <?php endif; ?> 
                    <?php $image = get_sub_field( 'subpage_konj12_carousel_repeater_image' ); ?>
                    <img src ="<?php echo $image['sizes']['konj-carousel']; ?>" class="img-responsive" />
                    <?php if( $counter == 0) : ?>
                    </a></li>
                    <?php endif; ?>
                    <?php if( $counter > 0) : ?>
                    </a></li>
                    <?php endif; ?>
                    <?php if( $counter >= 10) : ?>
                    </a></li>
                    <?php endif; ?>
                    <?php if( $counter >= 20) : ?>
                    </a></li>
                    <?php endif; ?>
                    <?php if( $counter >= 30) : ?>
                    </a></li>
                    <?php endif; ?>
                    <?php $counter++; ?>
                <?php endwhile;
            endif; ?>
        </ul>
        <div class="row">
            <div class="col-md-12" id="slider">
                <div class="col-md-12" id="carousel-bounding-box">
                    <div id="myCarousel" class="carousel slide">
                        <div class="carousel-inner">
                         <?php $counter1 = 0; ?>   
                         <?php if( have_rows( 'subpage_konj12_carousel_repeater') ) :
                            while( have_rows( 'subpage_konj12_carousel_repeater') ) :
                                the_row();?> 
                                <?php if( $counter1 == 0) : ?>
                                    <div class="active item" data-slide-number="<?php echo $counter1; ?>">
                                <?php endif; ?>
                                <?php if( $counter1 > 0)  : ?>
                                    <div class="item" data-slide-number="<?php echo $counter1; ?>">
                                <?php endif; ?> 
                                <?php $image = get_sub_field( 'subpage_konj12_carousel_repeater_image' ); ?>        
                                <center><img src ="<?php echo $image['sizes']['large']; ?>" class="img-responsive" /></center>
                                <?php if( $counter1 == 0) : ?>
                                    </div>
                                <?php endif; ?>
                                <?php if( $counter1 > 0)  : ?>
                                    </div>
                                <?php endif; ?>
                                <?php $counter1++; ?>
                            <?php endwhile;
                        endif; ?>    
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        </br>
        
    </div>
    </div> 
     <div class ="row">
        <div class ="col-md-12 col-lg-12 col-xs-6">
            <center><b><h3>* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *</h3></b></center>
        </div>
    </div>
    </br>
     <div class ="row">
        <div class ="col-md-12 col-lg-12 col-sm-12">
            <?php if( get_field( 'subpage_konj12_frame_check_2') ) :
                if( in_array( 'Sa okvirom', get_field( 'subpage_konj12_frame_check_2') ) ) : ?>
                    <table cellpadding="50" align="center" style ="border:5px solid black"><tr><td align="center">
                <?php endif;
            endif; ?>
            <?php the_field( 'subpage_konje12_text_fan' ); ?>
            <?php if( get_field( 'subpage_konj12_frame_check_2') ) :
                if( in_array( 'Sa okvirom', get_field( 'subpage_konj12_frame_check_2') ) ) : ?>
                    </td></tr></table>
                <?php endif;
            endif; ?>
        </div>
    </div>
    </br>
    </br>
    </div>
</div>
<script>
$('#myCarousel').carousel({
    interval: 6000
});

// handles the carousel thumbnails
$('[id^=carousel-selector-]').click( function(){
  var id_selector = $(this).attr("id");
  var id = id_selector.substr(id_selector.length -1);
  id = parseInt(id);
  $('#myCarousel').carousel(id);
  $('[id^=carousel-selector-]').removeClass('selected');
  $(this).addClass('selected');
});
$('[id^=carousel-selector2-]').click( function(){
  var id_selector = $(this).attr("id");
  var id = id_selector.substr(id_selector.length -2);
  id = parseInt(id);
  $('#myCarousel').carousel(id);
  $('[id^=carousel-selector-]').removeClass('selected');
  $(this).addClass('selected');
});
$('[id^=carousel-selector3-]').click( function(){
  var id_selector = $(this).attr("id");
  var id = id_selector.substr(id_selector.length -3);
  id = parseInt(id);
  $('#myCarousel').carousel(id);
  $('[id^=carousel-selector-]').removeClass('selected');
  $(this).addClass('selected');
});
$('[id^=carousel-selector4-]').click( function(){
  var id_selector = $(this).attr("id");
  var id = id_selector.substr(id_selector.length -4);
  id = parseInt(id);
  $('#myCarousel').carousel(id);
  $('[id^=carousel-selector-]').removeClass('selected');
  $(this).addClass('selected');
});

// when the carousel slides, auto update
$('#myCarousel').on('slid', function (e) {
  var id = $('.item.active').data('slide-number');
  id = parseInt(id);
  $('[id^=carousel-selector-]').removeClass('selected');
  $('[id^=carousel-selector-'+id+']').addClass('selected');
});
</script>
<?php get_footer(); ?>
