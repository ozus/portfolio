<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js <?php echo ($GLOBALS['rwd'] == true)? 'rwd': ''; ?>"> <!--<![endif]-->

<head <?php language_attributes(); ?>>
    <meta charset="<?php echo bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta name="author" content="Udruga Mogu" />
    <meta name="contact" content="udruga.mogu@gmail.com" />
    <meta name="copyright" content="Copyright (c)2006-2014 Udruga Mogu. All Rights Reserved." />
    <meta name="description" content="<?php echo bloginfo( 'description' ); ?>" />
    <meta name="keywords" content="udruga, mogu, osijek, terapijsko, jahanje, konji, sport, invlaliditet, poseben potrebe" />
    <title><?php bloginfo('name'); ?>||<?php wp_title(); ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_URI; ?>/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href ="<?php echo TEMPLATE_URI; ?>/css/Main.css" />
    <link rel="stylesheet" type="text/css"href="<?php echo TEMPLATE_URI; ?>/css/blueimp-gallery.min.css" />
    <link rel="sprite" type="image/png" href="<?php echo TEMPLATE_URI; ?>/sprites.png" />
    <script src="<?php echo TEMPLATE_URI; ?>/js/modernizr/modernizr-2.6.2.min.js"></script>
        <script src="<?php echo TEMPLATE_URI; ?>/js/modernizr/ietests.js"></script>    
        <script type="text/javascript">
            
            Modernizr.load([
                {
                    test: Modernizr.ie9,
                    yep: '<?php echo TEMPLATE_URI; ?>/css/ie9.css'                    
                },
                {
                    test: Modernizr.ie8,
                    yep: [
                        '<?php echo TEMPLATE_URI; ?>/css/ie9.css',
                        '<?php echo TEMPLATE_URI; ?>/css/ie8.css'
                    ]                    
                },
                {
                    test: Modernizr.mq('only all'),
                    nope: '<?php echo TEMPLATE_URI; ?>/js/respond/respond.js'
                }
            ]);
        </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo TEMPLATE_URI; ?>/js/jquery/jquery-1.11.1.min.js"><\/script>');</script>
    
    <script src="<?php echo TEMPLATE_URI; ?>/js/bootstrap/bootstrap.min.js"></script>
    <?php wp_head(); ?>
</head>

<body class="volonters">
<div id="header" class="volonters">
    <div class ="container">
       <div class="row">
           <div class ="col-xs-7 col-sm-7 col-md-7 col-lg-7 logoMargin">
               <img src ="<?php echo TEMPLATE_URI; ?>/logo.jpg" style="width:450px; height:65px" alt="Logo" />
           </div>
           <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 alignright buttonMaegin">
                <?php $id = get_page_id( 'Elementi' ); ?>
                <?php if( get_field( 'header_main_page_links', $id ) ) :
                    if( in_array( 'Glavna stranica', get_field( 'header_volonters_links', $id ) ) ) : ?> 
                        <a href="<?php echo get_permalink( get_page_id( 'O Nama') ); ?>" class="LinkButton1">Glavna</a>
                <?php endif; ?>
                <?php if( in_array( 'Volonteri', get_field( 'header_volonters_links', $id ) ) ) : ?> 
                        <a href="<?php echo get_permalink( get_page_id( 'Volonteri') ); ?>" class="LinkButton1">Volonteri</a>
                <?php endif; ?>
                <?php if( in_array( 'Aktualna dogadanja', get_field( 'header_volonters_links', $id ) ) ) : ?> 
                        <a href="<?php echo get_permalink( get_page_id( 'Aktualna Dogadanja') ); ?>" class="LinkButton1">Aktualna</a>
                <?php endif; ?>
            <?php endif; ?> 
           </div>
           <div class="row">
               <div class="alignright userMargin curvedBorder2">
                   <?php if( !isset( $_SESSION['user']) || !isset( $_SESSION['password'] ) ) : ?>
                   
                                <a href="<?php echo TEMPLATE_URI; ?>/login/login.php">Login</a>or<a href="<?php echo TEMPLATE_URI; ?>/login/register.php">Register</a>
                          
                   <?php endif; ?>
                   <?php if( isset( $_SESSION['user']) && isset( $_SESSION['password'] ) ) : ?>
                        Dobrodošao/la <a href="<?php echo TEMPLATE_URI; ?>/login/<?php echo $_SESSION['user']; ?>Loged.php" target="_blank"><?php echo $_SESSION['user']; ?></a>Odjavi se: <a href="<?php echo TEMPLATE_URI; ?>/login/logaut.php">Logout</a>
                   <?php endif; ?>
               </div>
           </div>
        </div>
        </br>
        <div class="row">
            <center><b><?php the_field( 'header_volonters_text', $id ); ?></b></center>
        </div>
        </br>
        <div class ="row">
            <?php $id = get_page_id( 'Elementi' ); ?>
            <div class ="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <img src="<?php echo TEMPLATE_URI; ?>/volonteri4.jpg" width="940" height="400" alt="header" /> 
            </div>
        </div>
    </div>
</div>  
<div class ="container"> 
    <div class ="row">
        <div class ="col-xs-12 col-md-12 col-lg-12">
<?php get_template_part( 'navigation', 'volonters' ); ?>
        </div>
    </div>
</div> 
