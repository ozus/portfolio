<?php
/*
 * Template Name: Predlozak Volonteri Prikaz Galerije
 */
?>
<?php get_header('volonters1'); ?>
<?php
$db= mysqli_connect("localhost", "root", "", "mogu");
If(mysqli_connect_errno()) :
   exit("Neuspjelo spajanje na bazu" . mysqli_connect_error());
endif;
$sql_query = 'SELECT img From gallery';
$result = mysqli_query($db, $sql_query); 
?>
<div class="container">
    <div class="row">
        <div id="links">
            <?php while( $row = mysqli_fetch_array( $result ) ) : ?>
            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                <a href="<?php echo TEMPLATE_URI; ?>/UploadImage/upload/<?php echo $row['img']; ?>" title="<?php echo $row['title']; ?>" ><img class="thumbnail" src="<?php echo TEMPLATE_URI . "/UploadImage/upload/" . $row['img']; ?>"  alt="Slika" width="230" height="200" /></a>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
                <div class="slides"></div>
                <h3 class="title"></h3>
                <a class="prev">‹</a>
                <a class="next">›</a>
                <a class="close">×</a>
                <a class="play-pause"></a>
                <ol class="indicator"></ol>
</div>
<script>  
document.getElementById('links').onclick = function (event) {
    event = event || window.event;
    var target = event.target || event.srcElement,
        link = target.src ? target.parentNode : target,
        options = {index: link, event: event},
        links = this.getElementsByTagName('a');
    blueimp.Gallery(links, options);
};
</script>
<?php mysqli_close($db); ?>
<?php get_footer('volonters'); ?>

