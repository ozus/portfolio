<?php
/*
 * Template Name: Predlozak Arhiva Volonteri
 */
?>

<?php get_header('volonters'); ?>

<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
    <?php query_posts( array(
                                    'post_type' => 'post',
                                    'category_name' => 'volonteri',
                                    'posts_per_page' => 10,
                                    'paged' => $paged, 
                                ));
?>

<div class ="container">
<?php while( have_posts() ) : the_post(); ?>
    
                <center><div class="row">
                    <div class ="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <table border="0" bgcolor="#E0E0E0" style="width:500px"><tr><td bgcolor="#E0E0E0">
                        <h3><b><div style="color:#000000; text-decoration:none"><a href ="<?php get_permalink(); ?>" target="blank"><?php the_title(); ?></a></div></b></h3>
                        </td></tr></table>
                    </div>
                </div>
                <div class ="row">
                    <table border="0" bgcolor="#000000" style="border-bottom:3px black solid; width:500px;">
                        <tr>
                            <td align="left" bgcolor="#E0E0E0" style="padding:5px">
                                 <?php echo get_the_date(); ?>
                            </td>
                            <td align="left" bgcolor="#E0E0E0" style="padding:5px">
                                 <?php the_author(); ?>
                            </td>
                            <td align="right" bgcolor="#E0E0E0" style="padding:5px">
                                 <?php comments_popup_link( 'nema komentara', '1 komentar', 'pogledaj komentare' ); ?>
                            </td>
                        </tr>
                    </table>    
                </div>
                <div class ="row">
                    <table border="0" style="width:500px" bgcolor="#000000">
                        <tr>
                            <td align="left" bgcolor="#E0E0E0" style="padding:10px">
                                <?php the_content(); ?>
                            </td>
                        </tr>
                    </table>
                </div></center>
        </br>
        </br>
        
<?php endwhile; ?>
<?php posts_nav_link( ' &#183; ', 'Prijašnja Stranica', 'Sljedeća Stranica' ); ?>
 </div> 
<?php wp_reset_query(); ?>
<?php get_footer('volonters'); ?>
