<?php 

include 'Sort.php';
include 'Max.php';
include 'Key.php';
include 'ValidateMatrix.php';


$matrix = json_decode($_GET['matrix']);

$error = (new ValidateMatrix) -> validate($matrix);

if($error == 0) {
	
	$sorted = (new Sort) -> sort($matrix);
	
	$solution[1] = $sorted[0];
	$solution[2] = $sorted[1];
	$solution[3] = $sorted[2];
	
	
	$maxes = (new Max) -> findMax($solution);
	$keys = (new Key) -> findKeys($solution, $maxes);
	
	$rawResult = array_merge($keys[0], $keys[1], $keys[2]); 
	$result = array_sum($rawResult);
	
	$ret = array($result, 0, $solution);
	echo json_encode($ret);

} else {
	$ret = array("Pleas check you input credentials; all inputs must be one digits numbers and there can be no empty fields!", 1);
	echo json_encode($ret);	
} 








