<?php

class Max {
	
	
	public function findMax(array $solution) {
		
		$values = $this -> findValues($solution);
		
		$maxes = array();
		foreach($values as $value) {
			if($value == $values[0]) {
				array_push($maxes, $value);
			}
		}
		
		return $maxes;
	}
	
	protected function findValues(array $sol) {
		
		$values = array();
		for($i = 1; $i <= count($sol); $i++) {
			for($j = 1; $j <= count($sol); $j++) {
				array_push($values, $sol[$i][$j]);
			}
		}
		
		rsort($values);
		
		return $values;
		
	}
	
}