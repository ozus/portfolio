<?php

class Key {
	
	public function findKeys(array $solution, array $maxes) {
		
		$keys1 = array_keys($solution[1], $maxes[0]);
		$keys2 = array_keys($solution[2], $maxes[0]);
		$keys3 = array_keys($solution[3], $maxes[0]);
		
		if(isset($keys1)) {
			array_walk($keys1, array($this, 'addToKey'), 1);
		}
		if(isset($keys2)) {
			array_walk($keys2, array($this, 'addToKey'), 2);
		}
		if(isset($keys3)) {
			array_walk($keys3, array($this, 'addToKey'), 3);
		}
	
		
		return array($keys1, $keys2, $keys3);
		
	}
	
	protected function addToKey(&$item, $key, $prefix) {
		$item= $item + $prefix;
	}
	
}