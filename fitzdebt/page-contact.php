
<?php $id = get_page_id( 'Contact' ); ?>

<div id="contact" class="section">
    <!-- Napravi i labele u formi i onda ih po difoltu sakrij, a za IE9 i niže ih vrati zato što oni ne podržavaju placeholdere -->
    <div class="container">
        <h2><?php the_field( 'contact_title', $id ); ?></h2>
        <p class="contactINTRO">
            <?php the_field( 'contact_content_text', $id ); ?>
        </p>
        <div class="row shadowDivider"></div>
        <div id="contact-response"></div>
        <form id="contact-form">
            <div class="row">
                <div class="col-md-6">
                    <label for="fitzdebt_name"><?php the_field( 'contact_name', $id ); ?> <span class="purple">&#42;</span></label>
                    <input class="text-input required" id="fitzdebt_name" name="fitzdebt_name" placeholder="Name *" type="text" />
                </div>
                <div class="col-md-6">
                    <label for="fitzdebt_email"><?php the_field( 'contact_e_mail', $id ); ?> <span class="purple">&#42;</span></label>
                    <input class="text-input required" id="fitzdebt_email"  name="fitzdebt_email" placeholder="E-mail *" type="email" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="fitzdebt_company"><?php the_field( 'contact_company', $id ); ?></label>
                    <input class="text-input" id="fitzdebt_company" name="fitzdebt_company" placeholder="Company" type="text"/>
                </div>
                <div class="col-md-6">
                    <label for="fitzdebt_phone"><?php the_field( 'contact_phone', $id ); ?></label>
                    <input class="text-input" id="fitzdebt_phone" name="fitzdebt_phone" placeholder="Phone Number" type="tel" />
                </div>
            </div>
            <div class="row msginput">
                <label for="fitzdebt_message"><?php the_field( 'contact_message', $id ); ?></label>
                <textarea class="text-input required" id="fitzdebt_message" name="fitzdebt_message" placeholder="Message *" rows="6"></textarea></div>
            <div class="contact-actions">
                <div class="required-text col-md-5 col-sm-5">All fields marked with (<span class="purple">&#42;</span>) are required to proceed.</div>
                <div class="col-md-7 col-sm-7">
                    <!--<button id="contact-form-reset-button" type="reset" class="button">Clear Form&nbsp;<i class="glyphicon glyphicon-remove"></i> </button>-->
                    <button id="contact-form-submit-button" type="button" class="button">Send Message&nbsp;<i class="glyphicon glyphicon-arrow-right"></i> </button>
                </div>
                <div class="clearfix"></div>
            </div>
        </form>
        <div class="row shadowDivider"></div>
        <div class="row">
            <div class="contactInfo">
                <?php if(have_rows( 'contact_repeater_adresses', $id ) ) :
                    while( have_rows( 'contact_repeater_adresses', $id ) ) :
                         the_row(); ?>
                         <p><?php the_sub_field( 'contact_repeater_adresses_adress' ); ?></p>    
                    <?php endwhile;
                endif; ?>
                       <ul> 
                <?php if( have_rows( 'contact_repeater_contact', $id ) ) :
                    while( have_rows( 'contact_repeater_contact', $id ) ) :
                         the_row(); ?>
                         <li><?php the_sub_field( 'contact_repeater_contact_form' ); ?>: <?php the_sub_field( 'contact_repeater_contact_number' ); ?></li>
                    <?php endwhile;
                endif; ?>
                       </ul>     
                       <ul>
                <?php if( have_rows ( 'contace_repeater_e_mail', $id ) ) :
                    while( have_rows( 'contace_repeater_e_mail', $id ) ) :
                         the_row(); ?>
                         <li><?php the_sub_field( 'contact_repeater_e_mail_department' ); ?>: <a href="mailto:<?php the_sub_field( 'contact_perepater_e_mail_mail' ); ?>" title="" target="_blank" ><?php the_sub_field( 'contact_perepater_e_mail_mail' ); ?></a></li>
                    <?php endwhile; 
                endif; ?>       
                        </ul>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery('#contact-form-submit-button').click(function() {
        jQuery.ajax({
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            type: 'POST',
            data: {
                'action': 'send_mail',
                'fitzdebt_company': jQuery('#fitzdebt_company').val(),
                'fitzdebt_name': jQuery('#fitzdebt_name').val(),
                'fitzdebt_email': jQuery('#fitzdebt_email').val(),
                'fitzdebt_phone': jQuery('#fitzdebt_phone').val(),
                'fitzdebt_message': jQuery('#fitzdebt_message').val()
            },
            success: function(response) {
                console.log(response);
                if (response.error == true) {
                    jQuery('#contact-response').html(response.message);
                } else if (response.error == false) {
                    jQuery('#contact-response').html(response.message);
                    jQuery('#contact-form').hide();
                }
            },
            error: function(errorThrown) {
                console.log(errorThrown);
            }
        });
    });
</script>