<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destination', function (Blueprint $table) {
            $table->increments('id');
            $table -> string('countrey', 255);
            $table -> string('city', 255);
            $table -> string('duration');
            $table -> dateTime('departure_date') -> nullable();
            $table -> dateTime('departure_time') -> nullable();
            $table -> datetime('arival_date') -> nullable();
            $table -> dateTime('arival_time') -> nullable();
            $table -> string('cost');
            $table -> string('code') -> unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destination');
    }
}
