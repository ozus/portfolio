<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('contry') -> create('contry', function (Blueprint $table) {
            $table->increments('id');
            $table -> string('contry_name', 255) -> unique();
            $table -> string('distance', 255);
            $table -> string('capital', 255);
            $table -> double('rating');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contry');
    }
}
