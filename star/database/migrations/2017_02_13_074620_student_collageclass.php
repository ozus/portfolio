<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentCollageclass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_colageclasses', function(Blueprint $table) {
        	
        	$table -> integer('students_id') -> unsigned() -> nullable();
        	$table -> foreign('students_id') -> references('id') -> on('students') -> onDelete('cascade');
        	
        	$table -> integer('colageclasses_id') -> unsigned() -> nullable();
        	$table -> foreign('colageclasses_id') -> references('id') -> on('colageclasses') -> onDelete('cascade');
        	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_collageclasses');
    }
}
