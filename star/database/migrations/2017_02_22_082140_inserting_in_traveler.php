<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertingInTraveler extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	
    	$favorites1 = array('Mexico', 'Australia', 'Japan');
    	$favorites2 = array('SAD', 'France', 'Greece');
    	$favorites3 = array('Argentina', 'Brazil', 'Peru');
    	
    	$data1 = [
    				
    			'email' => 'luka@paljetak.com',
    			'name' => 'Luka',
    			'lname' => 'Paljetak',
    			'address' => 'Zgubidanska 4',
    			'city' => 'city 4',
    			'creditcard' => bcrypt('111-111-111-4'),
    			'favorits' => serialize($favorites1),
    	];
    	
    	$data2 = [
    	
    			'email' => 'jagoda@truhelka.com',
    			'name' => 'Jagoda',
    			'lname' => 'Truhelka',
    			'address' => 'Zgubidanska 5',
    			'city' => 'city 5',
    			'creditcard' => bcrypt('111-111-111-5'),
    			'favorits' => serialize($favorites2),
    	];
    	
    	
    	$data3 = [
    	
    			'email' => 'emilia@eirhart.com',
    			'name' => 'Emilia',
    			'lname' => 'Eirhart',
    			'address' => 'Zgubidanska 6',
    			'city' => 'city 6',
    			'creditcard' => bcrypt('111-111-111-6'),
    			'favorits' => serialize($favorites3),
    	];
    	
    	
    	$data4 = [
    			 
    			'email' => 'tin@ujevic.com',
    			'name' => 'Tin',
    			'lname' => 'Ujevic',
    			'address' => 'Zgubidanska 7',
    			'city' => 'city 7',
    			'creditcard' => bcrypt('111-111-111-7'),
    			'favorits' => serialize($favorites3),
    	];
    	
    	DB::table('traveler') -> insert(array($data1, $data2, $data3, $data4));
    	
    		
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('traveler') -> where('email', '=', 'luka@paljetak.com') -> orWhere('email', '=', 'jagoda@truhelka.com')
                              -> orWhere('email', '=', 'emilia@eirhart.com') -> orWhere('email', '=', 'tin@ujevic.com') 
                              -> delete();
    }
}
