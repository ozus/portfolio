<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColageClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colageclasses', function (Blueprint $table) {
            $table->increments('id');
            $table -> string('classname');
            $table -> string('teacher');
            $table -> integer('year', FALSE);
            $table -> integer('semester', FALSE);
            $table -> string('classcode') -> unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colageclasses');
    }
}
