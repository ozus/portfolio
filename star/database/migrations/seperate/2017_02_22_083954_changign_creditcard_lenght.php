<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangignCreditcardLenght extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('traveler', function(BluePrint $table) {
        	$table -> string('creditcard', 255) -> change();
        }) ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('traveler', function(BluePrint $table) {
        	$table -> string('creditcard', 13) -> change();
        }); 
    }
}
