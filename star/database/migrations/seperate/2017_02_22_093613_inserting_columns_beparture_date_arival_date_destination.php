<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertingColumnsBepartureDateArivalDateDestination extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('destination', function(BluePrint $table) {
    		$table -> date('departure_date') -> nullable();
    		$table -> date('arival_date') -> nullable();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('destination', function(BluePrint $table) {
    		$table -> dropColumn(['departure_date', 'arival_date']);
    	});
    }
}
