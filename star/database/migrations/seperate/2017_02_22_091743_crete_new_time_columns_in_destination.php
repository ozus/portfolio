<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreteNewTimeColumnsInDestination extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('destination', function(BluePrint $table) {
    		$table -> time('departure_time') -> nullable();
    		$table -> time('arival_time') -> nullable();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('destination', function(BluePrint $table) {
    		$table -> dropColumn(['departure_time', 'arival_time']);
    	});
    }
}
