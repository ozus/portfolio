<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteDepartureDateArivalDateDestination extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('destination', function(BluePrint $table) {
    		$table -> dropColumn(['departure_date', 'arival_date']);
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('destination', function(BluePrint $table) {
    		$table -> dateTime('departure_date') -> nullable();
    		$table -> dateTime('arival_date') -> nullable();
    	});
    }
}
