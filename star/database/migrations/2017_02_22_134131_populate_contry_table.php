<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateContryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {	
    	
    	
    	$data1 = [
    			
    			'contry_name' => 'China',
    			'distance' => '2356.32 km',
    			'capital' => 'Peking',
    			'rating' => 7.5
    			
    	];
    	
    	
    	$data2 = [
    			 
    			'contry_name' => 'Japan',
    			'distance' => '2957.32 km',
    			'capital' => 'Tokio',
    			'rating' => 9.0
    			 
    	];
    	
    	
    	$data3 = [
    			 
    			'contry_name' => 'France',
    			'distance' => '1500.43 km',
    			'capital' => 'Paris',
    			'rating' => 9.8
    			 
    	];
    	
    	
    	$data4 = [
    			 
    			'contry_name' => 'Australia',
    			'distance' => '4101.20 km',
    			'capital' => 'Canberra',
    			'rating' => 6.5
    			 
    	];
    	
    	
    	
    	$data5 = [
    			 
    			'contry_name' => 'England',
    			'distance' => '2010.45 km',
    			'capital' => 'London',
    			'rating' => 5.5
    			 
    	];
    	
    	
    	
    	$data6 = [
    			 
    			'contry_name' => 'Russia',
    			'distance' => '2500.09 km',
    			'capital' => 'Moscow',
    			'rating' => 2.5
    			 
    	];
    	
    	
    	
        DB::connection('contry') -> table('contry') -> insert(array($data1, $data2, $data3, $data4, $data5, $data6));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::connection('country') -> where('country_name', '=', 'China') -> orWhere('country_name', '=', 'Japan') 
        						  -> orWhere('country_name', '=', 'France') -> orWhere('country_name', '=', 'Australia')
        						  -> orwhere('country_name', '=', 'England') -> orWhere('country_name', '=', 'Russia')
        						  -> delete();
    }
}
