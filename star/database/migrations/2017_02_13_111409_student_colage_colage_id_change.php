<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentColageColageIdChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students_colageclasses', function($table) {
        	$table -> renameColumn('colageclasses_id', 'colage_class_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('students_colageclasses', function($table) {
    		$table -> renameColumn('colage_class_id', 'colageclasses_id');
    	});
    }
}
