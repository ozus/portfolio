<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertingIntoDestination extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	
    	$data1 = [
    			
    			'countrey' => 'France',
    			'city' => 'Paris',
    			'duration' => '2',
    			'departure_date' => '2017-09-28',
    			'departure_time' => '12:00:30',
    			'arival_date' => '2017-09-29',
    			'arival_time' => '09:11:12',
    			'cost' => '2100,26',
    			'code' => '1'
    			
    	];
    	
    	
    	$data2 = [
    			 
    			'countrey' => 'Australia',
    			'city' => 'Canberra',
    			'duration' => '4',
    			'departure_date' => '2018-10-30',
    			'departure_time' => '12:30:30',
    			'arival_date' => '2018-10-31',
    			'arival_time' => '10:11:12',
    			'cost' => '5600,26',
    			'code' => '2'
    
    	];
    	
    	
    	$data3 = [
    			 
    			'countrey' => 'SAD',
    			'city' => 'New York',
    			'duration' => '6',
    			'departure_date' => '2018-11-17',
    			'departure_time' => '12:10:30',
    			'arival_date' => '2018-11-19',
    			'arival_time' => '10:11:12',
    			'cost' => '8900,26',
    			'code' => '3'
    
    	];
    	
    	
    	$data4 = [
    			 
    			'countrey' => 'Egypt',
    			'city' => 'Kayro',
    			'duration' => '1',
    			'departure_date' => '2018-12-30',
    			'departure_time' => '12:00:30',
    			'arival_date' => '2018-12-31',
    			'arival_time' => '11:11:12',
    			'cost' => '4900,26',
    			'code' => '4'
    
    	];
    	
    	DB::table('destination') -> insert(array($data1, $data2, $data3, $data4));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('destination') -> where('code', '=', '1') -> orWhere('code', '=', '2') -> orWhere('code', '=', '3')
        						 -> orWhere('code', '=', '4') -> delete();
    }
}
