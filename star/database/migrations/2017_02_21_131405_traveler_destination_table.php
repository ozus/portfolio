<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TravelerDestinationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traveler_destination', function(Blueprint $table) {
        	
        	$table -> integer('traveler_id') -> unsigned() -> nullable();
        	$table -> foreign('traveler_id') -> references('id') -> on('traveler') -> onDelete('cascade');
        	
        	$table -> integer('destination_id') -> unsigned() -> nullable();
        	$table -> foreign('destination_id') -> references('id') -> on('destination') -> onDelete('cascade');
        	
        	$table -> timestamps();
        	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traveler_destination');
    }
}
