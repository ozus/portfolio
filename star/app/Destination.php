<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    protected $table = 'destination';
    
    public function traveler() {
    	return $this -> belongsToMany('App\Traveler', 'traveler_destination', 'destination_id', 'traveler_id') 
    	-> withTimestamps();
    }
}
