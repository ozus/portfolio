<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Traveler extends Model
{
    protected $table = 'traveler';
    
    public function destination() {
    	return $this -> belongsToMany('App\Destination', 'traveler_destination', 'traveler_id', 'destination_id') 
    	-> withTimestamps();;
    }
}
