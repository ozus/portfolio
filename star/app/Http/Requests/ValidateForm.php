<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Factory as ValidationFactory;

use Carbon\Carbon;

class ValidateForm extends FormRequest
{
	
	public function __construct(ValidationFactory $factory) {
		
		$factory -> extend('to_old', function($attribute, $value, $parameters) {
			
			$now = Carbon::now();
			$date = $value;
			$date_fromated = strtotime($date);
			
			$year = date('Y', $date_fromated);
			$month = date('m', $date_fromated);
			$day = date('j', $date_fromated);
			
			$dateCarbon = Carbon::create($year, $month, $day, 0);
			$dateCarbon -> toDateString();
			
			if($now -> diffInYears($dateCarbon) > $parameters[0]) {
				return false;
			} else {
				return true;
			}
		});
		
		$factory -> replacer('to_old', function($message, $attribute, $value, $parameters) {
			
			return str_replace(':max', $parameters[0], $message);
			
		});
		
		
		$factory -> extend('same_as', function($attribute, $value, $parameters) {
			
			$field1 = $value;
			
			
			if($this -> has($parameters[0])) {
				$field2 = $this -> input($parameters[0]);
			} else {
				$field2 = $value;
			}
			
			
			
			if($field1 == $field2) {
				return true;
			} else {
				return false;
			}
			
			
		}); 
		
		$factory -> replacer('same_as', function($message ,$attribute, $value, $parameters) {
			
			return str_replace(':size', $parameters[0], $message);
			
		});
		
	}
	
	
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists_students:students',
        	'password' => 'required|Numeric|Min:1|Max:9',	
        	'hobys' => 'required|alpha_array',
        	'hobys.games' => 'Alpha',
        	'date' => 'required|Date|min_age:18|to_old:100',
        	'code1' => 'required|same_as:code2|Numeric',
        	'code2' => 'required|Numeric'	
        ];
    }
    
    
    public function messages() {
    	return [
    		'required' => ':attribute must be entered!',
    		'email' => ':attribute must be valid email address!',
    		'exists_students' => 'Value of :attribute dose not exists in database :size!',
    		'numeric' => ":attribute can contain only numbers!",
    		'password.min' => ':attribute must be grater then :min',
    		'password.max' => ':attribute can not be grater then :max',
    		'alpha_array' => ':attribute can contain only latters!',
    		'alpha' => ':attribut can cobntain only latters',
    		'date' => ':attribute must be valid date format!',
    		'min_age' => 'You cant be less then :min years old!',
    		'to_old' => 'You cant be older then :max years',
    		'same_as' => 'Fields :attribute and :size must match'	
    	];
    }
}
