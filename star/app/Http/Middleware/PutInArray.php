<?php

namespace App\Http\Middleware;

use Closure;

class PutInArray
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	 $data = array();
    	 if($request -> has('data1') || $request -> has('data2') ) {
    		$data['data1'] = $request -> data1;
    		$data['data2'] = $request -> data2;
    		$request -> merge(['data' => $data]);
    		
    	}  
    	
        return $next($request);
    	
    }
}
