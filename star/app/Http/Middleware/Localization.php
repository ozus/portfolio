<?php

namespace App\Http\Middleware;

use App;
use Closure;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {	
    	
    	$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
  
    		App::setLocale($language);
    	
        return $next($request);
    }
}
