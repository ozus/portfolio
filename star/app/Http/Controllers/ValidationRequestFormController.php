<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ValidateForm;
use Illuminate\Routing\Controller;


/**
 * Evaluate incoming request.
 *
 * @param  ValidateForm  $request
 * @return Response
 */

class ValidationRequestFormController extends Controller
{
   	
	public function postValidation(ValidateForm $request) {
		
		$email = $request -> input('email');
		$date = $request -> input('date');
		
		echo $date . "<br>";
		echo $email . "<br>";
		
	}
	
}

