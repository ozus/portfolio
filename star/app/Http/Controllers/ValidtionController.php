<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;



class ValidtionController extends Controller
{
	
	public function __construct() {
		$this -> middleware('inArray');
	}
	
	public function validation(Request $request) {
	

		$messages = [
				'required' => ':attribute field is requiered!',
				'numeric' => 'The :attribute must contain only numeric characters.',
				'password.min' =>  'The :attribute must be minimaly :min',
				'email.min' => 'The :attribute must have minimaly :min characters.',
				'email.max' => 'The :attribute can not have more then :max characters.',
				'password.max' => 'The :attribute can not be larger then :max',
				'email' => 'We need you to provide you email in Email field.',
				'exists_students' => 'Data for :attribute dose not exist in database',
				
				'alpha_array' => 'Field :attribute must contain only alpha characters',
				'num_array' => 'Field :attribute must contain only numeric characters',
				'data.data1.required' => 'The field pet is required',
				'min_age' => 'You cant be younger then :min years!'
		];
		 
		$validator = Validator::make($request -> all(), [
	
				'email' => 'required|Min:2|Max:30|Email|exists_students:students|denied',
				'password' => 'nullable|Numeric|Min:4|Max:40',
				'hobys' => 'required|alpha_array',
				'data' => 'nullable|alpha_array',
				'data.data1' => 'required',
				'data.data2' => 'nullable',
				'date' => 'required|Date|min_age:18'
    
		], $messages);
		 
		$validator -> sometimes('desc', 'required|Max:600', function($request) {
			return $request -> data['data2'] != null;
		});
		
		 
		if($validator -> fails()) {
			return redirect('/validation-form') -> withErrors($validator) -> withInput();
		}
		/* $data = array();
		$data = $request -> input('data');
		$email = $request -> input('email');
		$data1 = $request -> input('data1');
		echo $email;
		foreach($data as $dat) {
			echo $dat;
		} */
		$data = $request -> input('data');
		$date = $request -> input('date');
		echo $date . "<br>";
		
		$date_date_time = strtotime($date);
		
		$year = date("Y", $date_date_time);
		echo $year . "<br>";
		
		$month = date('m', $date_date_time);
		echo $month . "<br>";
		
		$day = date('j', $date_date_time);
		echo $day . "<br>";
		
		//return view('after-validate') -> with('data', $data);
	
	}
}
