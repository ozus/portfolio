<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\ColageClass;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use URL;
use DB;
use File;
use App\Mail\TestMail;
use Illuminate\Support\Facades\Mail;


class StudentController extends Controller
{
	
	protected $colageclass;
	
	public function __construct(ColageClass $colageclass) {
		$this -> colageclass = $colageclass;
	}
	
    public function getInsertForm() {
    
    	return view('student.insert-student-form');
    
	}
	
	
	public function insertStudent(Request $request) {
		
		$this -> validate($request, [
			
			'name' => 'required|Alpha|Min:2|Max:30',
			'lname' => 'required|Alpha|Min:2|Max:30',
			'email' => 'required|Email|Unique:students',
			'password' => 'required|Min:4|Max:40|Confirmed',
			'password_confirmation'	=> 'required|Min:4|Max:40'		
		]);
		
		$email = $request -> email;
		
		if(!Student::where('email', '=', $email ) -> exists()) {
			
			$student = new Student();
			
			$student -> name = $request -> name;
			$student -> lname = $request -> lname;
			$student -> email = $request -> email;
			$student -> password = bcrypt($request -> passwoord);
			
			$student -> save();
			
			return view('student.insert-student-form') -> with('message', 'student successfulyl created') -> with('errorMessage', ''); 
			
		} else {
			return view('student.insert-student-form') -> with('errorMessage', 'email address already exists!') -> with('message', '');
		}
		
	}
	
	
	public function getNewClassForm() {
		
		return view('student.insert-class-form');
		
	}
	
	
	public function insertClass(Request $request) {
		
		$this -> validate($request, [
			'classname' => 'required|Min:4|Max:40', //|Unique:colageclasses
			'teacher' => 'required|AlphaDash|Min:1|Max:40',
			'year' => 'required|Numeric|Min:1|Max:6',
			'semester' => 'required|Numeric|Min:1|Max:6',
			'classcode'	=> 'required|Numeric|Min:0|Max:10', //|Unique:colageclasses
		]);
		
		$classcode = $request -> classcode;
		$classname = $request -> classname;
		
		if(!ColageClass::where('classcode', '=', $classcode) -> exists() && !ColageClass::where('classname', '=', $classname) -> exists()) {
			
			$this -> colageclass -> classname = $request -> classname;
			$this -> colageclass -> teacher = $request -> teacher;
			$this -> colageclass -> year = $request -> year;
			$this -> colageclass -> semester = $request -> semester;
			$this -> colageclass -> classcode = $request -> classcode;
			
			$this -> colageclass -> save();
			
			return view('student.insert-class-form') -> with('message', 'Class successfully added!') -> with('errorMessage', '');
			
		} else {
			
			return view('student.insert-class-form') -> with('message', '') -> with('errorMessage', 'Class with that Code or Name already exists!');
			
		}	
	}
	
	
	
	public function getInlitClassForm() {
		return view('student.classes-for-student');
	}
	
	
	public function classForStudent(Request $request) {
		
		$this -> validate($request, [
			
			'email' => 'required|Email',
			'classes' => 'required'				
		]);
		
		$email = $request -> email;
		$classes = $request -> classes;
		
		if(Student::where('email', '=', $email) -> exists() && ColageClass::where('classname', '=', $classes) -> exists() ) {
			
			$student = Student::where('email', '=', $email ) -> get();
			$relations = $student[0] -> collageClass() -> get();
			$class = ColageClass::where('classname', "=", $classes) -> get();
			if($relations -> contains($class[0])) {
				return redirect('/student/inlist-class') -> with('errorMessage', 'Class is already added!');
			} else {
				$student[0] -> collageClass() -> save($class[0]);
			}
			return view('student.classes-for-student') -> with('message', 'Class successfully added for student!') -> with('errorMessage', '');
		
		} else {
			return view('student.classes-for-student') -> with('message', '') -> with('errorMessage', 'Student or Class dose not exist!');
		}
	}
	
	
	
	public function getInlitStudentForm() {
		return view('student.students-for-class');
	}
	
	
	public function studentForClass(Request $request) {
		
		$this -> validate($request, [
					
				'classname' => 'required',
				'students' => 'required'
		]);
		
		$classname = $request -> classname;
		$students = $request -> students;
		
		if(ColageClass::where('classname', '=', $classname) -> exists() && Student::where('email', '=', $students) -> exists() ) {
				
			$class = ColageClass::where('classname', '=', $classname ) -> get();
			$relations = $class[0] -> student() -> get();
			$student = Student::where('email', "=", $students) -> get();
			if($relations -> contains($student[0])) {
				return redirect('/student/inlist-studnet') -> with('errorMessage', 'Student is already added!');
			} else {
				$student[0] -> collageClass() -> save($class[0]);
			}
			return view('student.students-for-class') -> with('message', 'Student successfully added to class!') -> with('errorMessage', '');
		
		} else {
				
			return view('student.students-for-class') -> with('message', '') -> with('errorMessage', 'Student or Class dose not exist!');
		}	
	}
	
	
	
	public function getFormNewClassForStudent() {
		return view('student.make-class-for-student');
	}
	
	
	public function makeClassForStudent(Request $request) {
		
		$this -> validate($request, [
				
				'email' => 'required|Email',
				'classname' => 'required|Min:4|Max:40|Unique:colageclasses', //|Unique:colageclasses
				'teacher' => 'required|Alpha|Min:1|Max:40',
				'year' => 'required|Numeric|Min:1|Max:6',
				'semester' => 'required|Numeric|Min:1|Max:6',
				'classcode'	=> 'required|Numeric|Min:0|Max:10|Unique:colageclasses', //|Unique:colageclasses
				
		]);
		
		$email =$request -> email;
		$classname = $request -> classname;
		$teacher = $request -> teacher;
		$year = $request -> year;
		$semester = $request -> semester;
		$classcode = $request -> classcode;
		
		if(Student::where('email', '=', $email) -> exists() && !ColageClass::where('classname', '=', $classname) -> exists() && !ColageClass::where('classcode', '=', $classcode) -> exists() ) {
			
			$students = Student::where('email', '=', $email) -> get();
			
			$this -> colageclass -> classname = $classname;
			$this -> colageclass -> teacher = $teacher;
			$this -> colageclass -> year = $year;
			$this -> colageclass -> semester = $semester;
			$this -> colageclass -> classcode = $classcode;
			
			$students[0] -> collageClass() -> save($this -> colageclass);
			
			return view('student.make-class-for-student') -> with('message', 'Class successfuly created and added to student') -> with('errorMessage', '');
			
		} else {
			return view('student.make-class-for-student') -> with('message', '') -> with('errorMessage', 'No such a student or class arleady exists!!');
		}
		
	}
	
	
	
	public function getMiltipleForm(Request $request) {
		
		$students = Student::all();
		
		
		$DBRelations = array();
		foreach($students as $student) {
			$relation = $student -> collageClass() -> get();
			array_push($DBRelations, $relation);
		}
		
		
		$classes = ColageClass::all();
		
		$request -> session() -> put('students', $students);
		$request -> session() -> put('classes', $classes);
		
		return view('student.multiclass-for-multistudent') -> with('request', $request) -> with('errorMessage', '');
	}
	
	
	public function insertMultiple(Request $request) {
		
		$this -> validate($request, [
				
			'students' => 'required',
			'classes' => 'required',	
				
		]);
		
		$students = array();
		$classes = array();
		$classes = $request -> classes;
		$students = $request -> students;
		
		/* var_dump($classes);
		echo "<br>";
		var_dump($students); */
		
		
		$DBStudents = array();
		foreach($students as $student) {
			$DBStudent = Student::where('email', 'dummy') -> orWhere('email', $student) -> get();
			array_push($DBStudents, $DBStudent);
		}
		//var_dump($DBStudents);
		
		
		
		$DBrealtions = array();
		foreach($DBStudents as $DBStudent) {
			foreach($DBStudent as $DBStu) {
				$realtions = $DBStu -> collageClass() -> get();
				array_push($DBrealtions, $realtions);
			}
		}
		//var_dump($DBrealtions);
		
	
		$DBClasses = array();
		foreach($classes as $class) {
			$DBClass = ColageClass::where('classname', 'gz47zwtzt6490zjwjš049zuj') -> orWhere('classname', $class) -> get();
			array_push($DBClasses, $DBClass);
		}
		//var_dump($DBClasses);
		
		
		
		foreach($DBrealtions as $relation) {
			foreach($DBClasses as $DBClass) {
				foreach($DBClass as $DBCla){
					if($relation -> contains($DBCla)) {
						return redirect() -> route('multipleForm') -> with('errorMessage', 'student already participating in some of classes you tryin to add') -> with('request', $request);
					}	
				}
			}
		}
		
		foreach($DBStudents as $DBStudent) {
			foreach($DBStudent as $DBStu) {
				foreach($DBClasses as $DBClass) {
					foreach($DBClass as $DBCla) {
						$DBStu -> collageClass() -> save($DBCla);
					}
				}
			}
		}
	
		return view('student.multiclass-for-multistudent') -> with('message', 'Inlisting was successfull') -> with('errorMessage', '') -> with('request', $request);
		
	
	
	
	}
	
	
	public function getStudentUpdateFrom(Request $request) {
		
		$students = Student::select('email', 'name', 'lname','password') -> get();
		
		$request -> session() -> put('students', $students);
		
		return view('student.sudent-update') -> with('request', $request);
	}
	
	
	
	public function updateStudent(Request $request) {
		$this -> validate($request, [
			'email' => 'required',
			'name' => 'nullable|Alpha|Min:2|Max:30',
			'lname' => 'nullable|Alpha|Min:2|Max:30',
			'password' => 'nullable|Min:2|Max:40'	
		]);
		
		$email = $request -> email;
		
		$student = Student::where('email', '=', $email) -> get();

		$name = $request -> input('name');
		$lname = $request -> input('lname');
		$password = $request -> input('password'); 
		
		var_dump($name);
		
		//var_dump($student[0] -> name);

		 if(Student::where('name', '=', $name) -> orWhere('lname', '=', $lname) -> orWhere('password', '=', $password) -> exists()) {
			return view('student.sudent-update') -> with('request', $request) -> with('message', '') -> with('errorMessage', 'Some of sate are same as current values of student!');
		} else {
			if($name != NULL) {
				$student[0] -> name = $name;
			}
			if($lname != NULL) {
				$student[0] -> lname = $lname;
			}
			if($password != NULL) {
				$student[0] -> password = $password;
			}
			
			$student[0] -> save();
			
			return view('student.sudent-update') -> with('request', $request) -> with('message', 'Successfully updated student details!') -> with('errorMessage', '');
			
		} 
		
	}
	
	
	public function getClassUpdateFrom(Request $request) {
		
		$classes = ColageClass::select('classname') -> get();
		
		$request -> session() -> put('classes', $classes);
		
		return view('student.class-update') -> with('request', $request);
		
	}
	
	
	public function updateClass(Request $request) {
		
		$this -> validate($request, [
			
			'classname_old' => 'required',
			'classname' => 'nullable|Min:2|Max:30|Unique:colageclasses',
			'teacher' => 'nullable|Min:2|Max:30|Alpha',	
			'year' => 'nullable|Numeric|Min:1|Max:6',
			'semester' => 'nullable|Numeric|Min:1|Max:6'		
		]);
		
		$classname_old = $request -> classname_old;
		
		$classes = ColageClass::where('classname', '=', $classname_old) -> get();
		//var_dump($classes[0] -> teacher);
		
		 $classname = $request -> input('classname');
		$teacher = $request -> input('teacher');
		$year = $request -> input('year');
		$semester = $request -> input('semester');
		
		if($classes[0] -> teacher == $teacher || $classes[0] -> year == $year || $classes[0] -> semester == $semester) {
			Input::flash();
			return view('student.class-update') -> with('request', $request) -> with('errorMessage', 'Some of data you tryin to input already exists!') -> with('message', '') -> withInput($request -> all());
			
		} else {
			
			if($classname != NULL) {
				$classes[0] -> classname = $classname;
			}
			if($teacher != NULL) {
				$classes[0] -> teacher = $teacher;
			}
			if($year != NULL) {
				$classes[0] -> year = $year;
			}
			if($semester != NULL) {
				$classes[0] -> semester = $semester;
			}
			
			$classes[0] -> save();
			
			return view('student.class-update') -> with('request', $request) -> with('message', 'successfully updated class') -> with('errorMessage', '');
		} 
		
	}
	
	
	
	
	public function getFormUpdateMulipleClass(Request $request) {
		
		$classes = ColageClass::select('classname') -> get();
		
		$request -> session() -> put('classes', $classes);
		
		return view('student.class-update-multiple') -> with('request', $request);		
	}
	
	
	
	public function updateMultipleClass(Request $request) {
		
		$this -> validate($request, [
			
			'classes' => 'required',
			'year' => 'nullable|Numeric|Min:1|Max:6',
			'semester' => 'nullable|Numeric|Min:1|Max:6'	
				
		]);
		
		$classesInput = array();
		$classesInput = $request -> classes;
		
		$classes = ColageClass::whereIn('classname', $classesInput) -> orderBy('classname', 'desc') -> get();
		
		$year = $request -> year;
		$semester = $request -> semester;
		
		/* foreach($classes as $class) {
			if($class -> year == $year || $class -> semester == $semester) {
				return redirect('/student/update-multiple-class') -> withInput() -> with('errorMessage', 'Some of classe you trying to update already contatins date you trying to input');
			}
		} */
		
		foreach($classes as $class) {
			if($year != NULL) {
				$class -> year = $year;
			}
			if($semester != NULL) {
				$class -> semester = $semester;
			}
			
			$class -> save();
		}
		
		return view('student.class-update-multiple') -> with('message', 'Classes successfully updated') -> with('request', $request) -> with('errorMessage', '');
	}
	
	
	public function getUpload() {
		
		$files = DB::table('files') -> select("*") -> get();
		$ext = array('.png', '.jpg', '.jpeg', '.gif');
		return view('upload', ['files' => $files, 'ext' => $ext]);
		
	}
	
	public function uploadFile(Request $request) {
		
		$extention = $request -> file('file') -> getClientOriginalExtension();
		$filename = $request -> file('file') -> getClientOriginalName();
		//echo $extention;
		 $filename1 = str_replace("." .$extention, "", $filename);
		
		$imgExtensions = array('png', 'jpg', 'jpeg', 'gif');
		$videoExtentions = array('mp4', 'wmv', 'mkv');
		$fileExtentions = array('pdf', 'txt', 'xdoc');
		$musicExtentions = array('mp3');
		
		 if(in_array($extention, $videoExtentions)) {
			
			$pathVideo = storage_path('app/uploads/videos/');
			
			$request -> file('file') -> move($pathVideo, $filename1);
			
			DB::table('files') -> insert(['path' => 'app/uploads/videos/' . $filename1, 'name' => $filename1, 'extention' => "." .$extention]);
			
		 } elseif(in_array($extention, $imgExtensions)) {
		 	
		 	//$pathImage = storage_path('app/uploads/images/');
		 	$pathImg = public_path('images');
		 		
		 	$pathImage = $request -> file('file') -> storeAS('uploads/images', $filename1);
		 	$request -> file('file') -> move($pathImg, $filename);
		 	
		 	DB::table('files') -> insert(['path' => 'app/uploads/images/' . $filename1, 'name' => $filename1, 'extention' => "." .$extention]);
		 
		 } elseif(in_array($extention, $fileExtentions)) {
		 	
		 	$pathFile = storage_path('app/uploads/documents/');
		 	
		 	$request -> file('file') -> move($pathFile, $filename1);
		 	
		 	DB::table('files') -> insert(['path' => 'app/uploads/documents/' . $filename1, 'name' => $filename1, 'extention' => "." .$extention]);
		 	
		 } elseif(in_array($extention, $musicExtentions)) {
		 	
		 	$pathMusic = storage_path('app/uploads/music/');
		 	
		 	$request -> file('file') -> move($pathMusic, $filename1);
		 	
		 	DB::table('files') -> insert(['path' => 'app/uploads/music/' . $filename1, 'name' => $filename1, 'extention' => "." .$extention]);
		 	
		 } else {
		 	$request -> session() -> flash('message', 'File you are tryin to add is not valid image or valid document or valid video');
		 	return back();
		 }
		 
		 $request -> session() -> flash('message', 'file succesfully added');
		 return back();
		
		/*  $path = $request -> file('file') -> storeAs('uploads', $filename1);
		if(in_array($extention, $imgExtensions)) {
			$pathImg = public_path('images');
			$request -> file('file') -> move($pathImg, $filename);
		}
		
		//$url = Storage::url($path);
	
		if(DB::table('files') -> insert(['path' => 'app/' . $path, 'name' => $filename1, 'extention' => "." .$extention])) {
			$request -> session() ->  flash('message', 'file successfuly added!');
			return back();
		} else {
			$request -> session() -> flash('message', 'file could not be added!');
		} */  
		
	}
	
	public function downloadFile($id) {
		$path = DB::table('files') -> select('path', 'name') -> where('id', '=', $id) -> get();
		
		return response() -> download(storage_path($path[0] -> path), $path[0] -> name);
	}
	
	public function deleteFile($id) {
		$file = DB::table('files') -> select('name', 'path', 'extention') -> where('id', '=', $id) -> get();
		File::delete(storage_path($file[0] -> path));
		if(File::exists(public_path('images/' .$file[0] -> name . $file[0] -> extention))) {
			File::delete(public_path('images/' .$file[0] -> name . $file[0] -> extention));
		}
		if(DB::table('files') -> where('id', '=', $id) -> exists()) {
			DB::table('files') -> where('id', '=', $id) -> delete();
		}
	}
	
	public function showFile($id) {
		
		$file = DB::table('files') -> select('path') -> where('id', '=', $id) -> limit(1) -> orderBy('path') -> get();
		
		return response() -> file(storage_path($file[0] -> path));
		
	}
	
	
	
	public function showStudents() {
		
		$students = Student::all();
		
		return view('student.show', ['students' => $students]);
		
	}
	
	
	
	//send mail 
	public function sendMail(Request $request, $id) {
		$address = $request -> input('mail');
		$message = $request -> input('message');
		
		$request -> session() -> put('msg', $message);
		
		$student = Student::select('name', 'lname', 'email') -> where('id', '=', $id) -> get();
		//dd($student);
		Mail::to($address) -> send(new TestMail($student[0]));
		$request -> session() -> forget('msg');
	}
}











