<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traveler;
use App\Destination;
use App\Contry;

class TravelerAppController extends Controller
{	
	
	public function getNewTravelerForm(Request $request) {
		
		$contrys = Contry::select('contry_name', 'id') -> get();
		
		$request -> session() -> put('contrys', $contrys);
		
		return view('travelerApp.new-traveler') -> with('request', $request);
		
	}
	
	public function addNewTraveler(Request $request) {
		
		//encription of credit card number
		
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$request -> session() -> put('nothing', 'nothing');
		session(['iv' => $iv]);
		
		$key = "key";
		$method = "AES-128-CBC";
		$cryptedCreditcard = openssl_encrypt($request -> input('creditcard'), $method, $key, 0, $iv);
	
		//serialization of array for db storage
		$serializedFavorits = serialize($request -> favorits);
		
		
		$traveler = new Traveler();
		
		$traveler -> email = $request -> input('email');
		$traveler -> name = $request -> input('name');
		$traveler -> lname = $request -> input('lname');
		$traveler -> address = $request -> input('address');
		$traveler -> city = $request -> input('city');
		$traveler -> creditcard = $cryptedCreditcard;
		$traveler -> favorits = $serializedFavorits;
		
		if($traveler -> save()) {
			return view('travelerApp.new-traveler') -> with('successMessage', 'succesfully added new Traveler') -> with('request', $request);
		} else {
			return redirect('/new-traveler') -> with('errorMessage', 'Adding traveler was not successfull!') -> with('request', $request);
		}
		
	}
	
	
	
	
	public function getTravelerListingForm() {
		
		return view('travelerApp.listing-traveler');
		
	}
	
	
	
	
	public function getTravelerInsertForme() {
		
		return view('travelerApp.new-traveler');
		
	}
	
	
	
	
    public function getBookingForm(Request $request) {
    
    	$travelers = Traveler::select('id', 'lname', 'name') -> get();
    	$destinations = Destination::select('id', 'countrey', 'city') -> get();
    	
    	
    	$request -> session() -> put('travelers', $travelers);
    	$request -> session() -> put('destinations', $destinations);
    	
    	return view('travelerApp.booking') -> with('request', $request);
    	
	}
	
	
	public function booking(Request $request) {
		
		$travelerIDs = $request -> input('travelers');
		$destiantionIDs = $request -> input('destinations');
		
		$travelers = Traveler::find($travelerIDs);
		$destiantions = Destination::find($destiantionIDs);
		
		foreach($destiantions as $destination) {
			foreach($travelers as $traveler) {
				if($destination -> traveler -> contains($traveler)) {
					return redirect('/traveler-booking') -> with('message', 'Traveler already booked!');
				}
			}
			
		}
		
		foreach($destiantions as $destination) {
			$destination -> traveler() -> attach($travelerIDs);
		}
		
		return view('travelerApp.booking') -> with('successMessage', 'Booking was successfull!') -> with('request', $request);
		
	}
	
	
	public function saveBooking(Request $request) {
		
		$travelerIDs = $request -> input('travelers');
		$destiantionIDs = $request -> input('destinations');
		
		$travelers = Traveler::find($travelerIDs);
		$destiantions = Destination::find($destiantionIDs);
		
		 foreach($destiantions as $destination) {
			foreach($travelers as $traveler) {
				if($destination -> traveler -> contains($traveler)) {
					return redirect('/traveler-booking') -> with('message', 'Traveler already booked!');
				}
			}
				
		} 
		
		
		foreach($destiantions as $destination) {
			$destination -> traveler() -> saveMany($travelers);
		}
		
		return view('travelerApp.bookng') -> with('message', 'Booking was successfull!');
	}
	
	
}
