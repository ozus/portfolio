<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ColageClass extends Model
{
    protected $table = 'colageclasses';
    
   
    public function student() {
    	return $this -> belongsToMany('App\Student', 'students_colageclasses', 'colage_class_id', 'students_id') -> withTimestamps(); 
    }
}
