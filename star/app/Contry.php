<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Contry extends Model
{
    protected $table = 'contry';
    
    protected $connection = 'contry';
    
    /**
     * Scope a query to get contrys and there IDs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeContrys($query) {
    	return $query -> select('contry_name', 'contry_id');
    }
}
