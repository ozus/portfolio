<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    
    
    public function collageClass() {
    	return $this -> belongsToMany('App\ColageClass', 'students_colageclasses', 'students_id', 'colage_class_id') -> withTimestamps();
    }
}
