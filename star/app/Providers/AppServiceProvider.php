<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Student;
use DB;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('exists_students', function($attribute, $value, $parameters) {
        	
        	//$students = Student::where($attribute, '=', $value) -> get();
        	
        	$students = DB::table($parameters[0]) -> where($attribute, '=', $value) -> get();
        	
        	
        	/* var_dump($request);
        	$sings = array();
        	foreach($students as $student) {
        		foreach($request -> all() as $req) {
        			if($student -> contains($req)) {
        				array_push($sings, 1);
        			} else {
        				array_push($sings, 0);
        			}
        		}
        	}
        	if(in_array(1, $sings)) {
        		return false;
        	} else {
        		return true;
        	}  */
        	if(!empty($students[0])) {
        		return true;
        	} else { 
        		return false;
        	}
        });
        
        Validator::replacer('exists_students', function($message, $attribute, $value, $parameters) {
        	return str_replace(':size', $parameters[0], $message);
        });
        
        
        Validator::extend('denied', function($attribute, $value, $parameters) {
        	
        	$parameters = array('fuck@fuck.com', "darn@darn.com", "cracker@cracker.com");
        	
        	if(in_array($value, $parameters)) {
        		return false;
        	} else {
        		return true;
        	}
        	
        }); 
        
        
       Validator::extend('alpha_array', function($attribute, $values, $parameters) {
       	
       		foreach($values as $value) {
       			if(ctype_alpha($value)) {
       				return true;
       			} else {
       				return false;
       			}
       		}
       });
       
       
       	Validator::extend('num_array', function($attribute, $values, $parameters) {
       		
       		foreach($values as $value) {
       			if(is_numeric($value)) {
       				return true;
       			} else {
       				return false;
       			}
       			
       		}
       		
       	});
       	
       Validator::extend("min_age", function($attribute, $value, $parameters) {
       	
       		$now = Carbon::now();
       		
       		$date = strtotime($value);
       		
       		$year = date("Y", $date);
       		$month = date("m", $date);
       		$day = date("j", $date);
       		
       		
       		$dateCarb = Carbon::create($year, $month, $day, 0);
       		$dateCarb -> toDateString();
       		
       		if($now -> diffInYears($dateCarb) < $parameters[0]) {
       			return false;
       		} else {
       			return true;
       		}
       	
       }); 
       
       Validator::replacer("min_age", function($message, $attribute, $value, $parameters) {
       		return str_replace(':min', $parameters[0], $message);
       }); 
       	
       		

        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
