<?php

use App\Student;
//use DB;
use App\Traveler;
use App\ColageClass;
use Carbon\Carbon;

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



	// Authentication Routes...
	$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
	$this->post('login', 'Auth\LoginController@login');
	$this->post('logout', 'Auth\LoginController@logout')->name('logout');
	
	// Registration Routes...
	$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
	$this->post('register', 'Auth\RegisterController@register');
	
	// Password Reset Routes...
	$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
	$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
	$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
	$this->post('password/reset', 'Auth\ResetPasswordController@reset');


	
	Route::get('/home', 'HomeController@index');
	
	//insert new students
	Route::get('/student/insert', 'StudentController@getInsertForm');
	Route::post('/student/insert', 'StudentController@insertStudent');
	
	//insert new classes
	Route::get('/student/insert-class', 'StudentController@getNewClassForm');
	Route::post('/student/insert-class', 'StudentController@insertClass');
	
	
	//Rlating Student with class
	Route::get('/student/inlist-class', 'StudentController@getInlitClassForm');
	Route::post('/student/inlist-class', 'StudentController@classForStudent');
	
	
	
	//Relating class with students
	Route::get('/student/inlist-studnet', 'StudentController@getInlitStudentForm');
	Route::post('/student/inlist-studnet', 'StudentController@studentForClass');
	
	
	//Making neaw class and relating it to student
	Route::get('/student/create-class-for-student', 'StudentController@getFormNewClassForStudent');
	Route::post('/student/create-class-for-student', 'StudentController@makeClassForStudent');
	
	
	//Multiple classes for multiple students
	Route::get('/student/multiple-insert', array('as' => 'multipleForm', 'uses' => 'StudentController@getMiltipleForm'));
	Route::post('/student/multiple-insert', 'StudentController@insertMultiple');
	
	//Upating student
	Route::get('student/update-student', 'StudentController@getStudentUpdateFrom');
	Route::post('student/update-student', 'StudentController@updateStudent');
	
	//Upating class
	Route::get('student/update-class', 'StudentController@getClassUpdateFrom');
	Route::post('student/update-class', 'StudentController@updateClass');
	
	//Update multiple classes
	Route::get('/student/update-multiple-class', 'StudentController@getFormUpdateMulipleClass');
	Route::post('/student/update-multiple-class', 'StudentController@updateMultipleClass');
	
	//Validation routes
	Route::get('/validation-form', function() {
		return view('/form-for-validation');
	});
	Route::post('/validation-form', 'ValidtionController@validation');
	
	
	//Validation routes with requestForm
	Route::get('/validation-from-reguest', function() {
		return view('form-for-validation-request');
	});
	Route::post('/validation-from-reguest', 'ValidationRequestFormController@postValidation');



//debuger
Route::get('/debug', function() {
	$emails = array('iva@ivic.com', 'mario@maric.com') ;
	$colageClasses =array('Theromdynamics', 'Quantum mechanics') ;
	$students = array();
	foreach($emails as $email) {
		$student = App\Student::where('email', 'dummy') -> orWhere('email', $email) -> get();
		array_push($students, $student);
	}
	//var_dump($students);
	//$classes = App\ColageClass::where(['classname' => 'Theromdynamics', 'classname' => 'Quantum mechanics']) -> get();
	$classes = array();
	foreach($colageClasses as $colageClass) {
		$class = App\ColageClass::where('classname', '999') -> orWhere('classname', $colageClass) -> get();
		array_push($classes, $class);
	}
	
	
	/* foreach($classes as $class) {
		foreach($class as $cla) {
			echo $cla -> classname;
		}
	}

	foreach($students as $student) {
		foreach($student as $stu) {
			echo $stu -> name;
		}
	}
	 */
	/* foreach($students as $student) {
		foreach ($student as $stu) {
			foreach($classes as $class) {
				foreach($class as $cla) {
					$stu -> collageClass() -> save($cla);
				}
			}
		}
	} */
	$relations = array();
	foreach($students as $student) {
		foreach ($student as $stu) {
			$relation = $stu -> collageClass() -> get();
			array_push($relations, $relation);
			}
		}
		//var_dump($relations);
		/* foreach($relations as $relation) {
			foreach($relations as $rel) {
				foreach($rel as $re) {
					echo $re -> classname;
				}
			}
		} */
		
		/* foreach($relations as $relation) {
			
				foreach($classes as $class) {
					foreach($class as $cla) {
						if($relation -> contains($cla)) {
							echo "true";
						} else {
							echo "false";
						}
					}
				}
			
		} */
	
	//var_dump($classes);
	//$realtions = $students[0] -> collageClass() -> get();
	//var_dump($realtions);
	/* foreach($students as $student) {
		echo $student -> name;
	}
	foreach($realtions as $relation) {
		echo $relation -> classname;
	} */
		
		
});

Route::get('/debug2', function() {
	
	$classes = ColageClass::where('teacher', '=', 'Brana') -> orWhere('teacher', '=', 'Dario') -> orderBy('teacher', 'desc') -> take(3) -> get();
	
	/* foreach($classes as $class) {
		foreach($class -> student as $student) {
			echo $student -> name;
		}
	} */
	
	/* $classess = $classes -> each(function($item, $key) {
		//echo $key;
		echo "<br>" . "<br>";
		echo $item;
	}); */
	//var_dump($classess);
	
	$forFilter = array('Mehanics', 'Electorodynamics');
	
	$filtered = $classes -> filter(function($value, $key) {
		
		$forFilter = array('Mehanics', 'Electorodynamics');
		$forFilterTeacher = array('Dario');
		
		if(!in_array($value -> classname, $forFilter) && !in_array($value -> teacher, $forFilterTeacher)) {
			return $value;
		}
		
		
	});
	//var_dump($filtered);
	
	foreach($filtered as $filter) {
		echo $filter -> teacher;
	}
	echo "<br>";
	
	$class = ColageClass::where('teacher', '=', 'Brana') -> orWhere('teacher', '=', 'Dario') -> get() -> first(function($value, $key) {
		
		return $value -> teacher != 'Brana';
		
	}) ;
	
	//var_dump($class);
	echo $class -> teacher;
	
	
	$classes = ColageClass::where('semester', '=', '1') -> get() -> map(function($item, $key) {
		return $item -> teacher;
	});
	
	//var_dump($classes);
	
	foreach($classes as $class) {
		echo $class;
	}
	
	echo "<br><br>";
	$classes2 = ColageClass::select('teacher') -> where('semester', '=', '1') -> get();
	var_dump($classes2);
	
	
	$students = Student::all();
	
	$merged = $students -> merge($classes2);
	
	foreach($students as $student) {
		echo $student . "<br>";
		echo "<br>";
	}
	
	
	foreach($classes2 as $class2) {
		echo $class2 . "<br>";
		echo "<br>";
	}
	
	foreach($merged as $merge) {
		echo $merge . "<br>";
		echo "<br>";
	}
	
});


//debuger3
Route::get('/debug3', function() {
	
	$students = DB::table('colageclasses') -> select('classname', 'teacher', 'year') -> selectRaw('count(*) as total') -> where('semester', '=', '3') 
	-> orwhere('semester', '=', 1) -> groupBy('teacher') -> groupBy('classname') -> groupBy('year') -> get();
	
	var_dump($students);
	
	foreach($students as $student) { 
		echo $student -> year . "   ";
		echo $student -> classname . "   ";
		echo $student -> total . "<br>";
	}
	
	$students_total = DB::table('colageclasses') -> select('semester', 'year') -> selectRaw('count(*) as students_total')  
												-> groupBy('year') -> groupBy('semester') -> get();
	var_dump($students_total);
	echo "<br>";
	foreach($students_total as $student) {
		echo $student -> year . "     " . $student -> semester . "      " . $student -> students_total . "<br>";
	}

			
	$years = array();
	$semesters = array();
	for($i = 1; $i <= 6; $i++) {
		
		 foreach($students_total as $student) {
			if($student -> year == $i) {
				$temp = array();
				switch ($student -> semester) {
					case 1:
						$temp[1] = $student -> students_total;
						break;
					case 2:
						$temp[2] = $student -> students_total;
						break;
					case 3:
						$temp[3] = $student -> students_total;
						break;
					case 4:
						$temp[4] = $student -> students_total;
						break;
					case 5:
						$temp[5] = $student -> students_total;
						break;
					case 6:
						$temp[6] = $student -> students_total;
						break;
				}
			
				 
				array_push($semesters, $temp);
				unset($temp);
				array_push($years, $i);
			} 
			
		} 
		
		
	}
	var_dump($semesters);
	echo "<br>";
	var_dump($years);
	 //$count_semesters = array_combine($years, $semesters);
	
	foreach($years as $year) {
		echo $year . "         ";
	}
	
	foreach ($semesters as $semester) {
		foreach($semester as $sem => $tot) {
			echo $tot . "     " ;
		}
	}
	
});


Route::get('/carbon', function() {
	
	$current = Carbon::now();
	echo "current time  " . $current . "<br>";
	
	$today = Carbon::today();
	echo "today  " . $today . "<br>";
	
	//custom created from time
	$customTime = Carbon::createFromTime('1', '28', '59', null);
	echo "custom time  " . $customTime . "<br>";
	
	//custom created form date
	$customDate = Carbon::createFromDate('1987', '09', '28', null);
	echo "custom date  " . $customDate . "<br>";
	
	//15 days from roday
	
	$plus15 = $current -> addDays(15);
	echo "15 days from today:  " . $current . "<br>";
	
	
	//general date-time manipuilation
	$td = Carbon::create('2011', '09', '18', 0);
	
	//converting to date and time string
	echo "date and time string:  " . $td -> toDateTimeString() . "<br>";
	//converting to date string
	echo "date string:  " . $td -> toDateString() . "<br>";
	//converting to time string
	echo "time string:" . $td -> toTimeString() . "<br>";
	//converting to formated date string
	echo "formated date string:  " . $td -> toFormattedDateString() . "<br>";
	//converting to day date time string
	$now = Carbon::now();
	echo "day date time string:  " . $now -> toDayDateTimeString() . "<br>";
	
	//getting values
	//day
	echo "day:  " . $now -> day . "<br>";
	//month
	echo "month:  " . $now -> month . "<br>";
	//year
	echo "year:  " . $now -> year . "<br>";
	//hour
	echo "hour:  " . $now -> hour . "<br>";
	//minutes
	echo "minutes:  " . $now -> minute . "<br>";
	//seconds
	echo "seconds:  " , $now -> second . "<br>";
	
	
	//setting values
	
	/* echo "SETTING VALUES" . "<br><br>";
	echo "setted day to 27" . $now -> day = 27 . "<br>";
	echo "setted month to 01" . $now -> month = 01 . "<br>";
	echo "setted year to 1987" . $now -> year = 1987 . "<br>";
	echo "setted hour to 23" . $now -> hour = 23 . "<br>";
	echo "setted minutes to 45" . $now -> minute = 45 . "<br>";
	echo "setted seconds to 59" . $now -> second = 50 . "<br>";
	 */
	
	
	echo "ADDIN AND SUBSTRACTING DATE AND TIME" . "<br><br>";
	//addin and substracting dates
	$datetime = Carbon::create('1987', '09', '28', '12', '30', '30');
	echo "starting date adn time:  " . $datetime . "<br>";
	
	//with years
	echo "plus 5 years:  " . $datetime -> addYears(5) . "<br>";
	echo "plus one year:  " . $datetime -> addYear() . "<br>";
	echo "minus 5 years:  " . $datetime -> subYears(5) . "<br>";
	echo "muinus one year: " . $datetime -> subYear() . "<br>";

	//with months
	echo "plus 5 months:  " . $datetime -> addMonths(5) . "<br>";
	echo "plus one month:  " . $datetime -> addMonth() . "<br>";
	echo "minus 5 months:  " . $datetime -> subMonths(5) . "<br>";
	echo "minus one month:  " . $datetime -> subMonth() . "<br>";
	
	
	//with days
	echo "plus 5 ays:  " . $datetime -> addDays(5) . "<br>";
	echo "plus one day:  " . $datetime -> addDay() . "<br>";
	echo "minus 5 days:  " . $datetime -> subDays(5) . "<br>";
	echo "minus one day:  " . $datetime -> subDay() . "<br>";
	
	
	//with weekday (startin new week if ledt less then 7 days till end of current)
	echo "plus 7 WeekDays:  " . $datetime -> addWeekdays(7) . "<br>";
	echo "plus one WeekDay:  " . $datetime -> addWeekday() . "<br>";
	echo "minus 7 WeekDays:  " . $datetime -> subWeekdays(5) . "<br>";
	echo "minus one WeekDay:  " . $datetime -> subWeekday() . "<br>";
	
	
	//with weeks
	echo "plus 5 weeks:  " . $datetime -> addWeeks(5) . "<br>";
	echo "plus one week:  " . $datetime -> addWeek() . "<br>";
	echo "minus 5 weeks:  " . $datetime -> subWeeks(5) . "<br>";
	echo "minus one week:  " . $datetime -> subWeek() . "<br>";
	
	//with hours
	echo $datetime->addHours(24). "<br>";                  // 2012-02-04 00:00:00
	echo $datetime->addHour(). "<br>";                     // 2012-02-04 01:00:00
	echo $datetime->subHour(). "<br>";                     // 2012-02-04 00:00:00
	echo $datetime->subHours(24). "<br>";                  // 2012-02-03 00:00:00
	
	
	//with minutes
	echo $datetime->addMinutes(61). "<br>";                // 2012-02-03 01:01:00
	echo $datetime->addMinute() . "<br>";                   // 2012-02-03 01:02:00
	echo $datetime->subMinute() . "<br>";                   // 2012-02-03 01:01:00
	echo $datetime->subMinutes(61). "<br>";                // 2012-02-03 00:00:00
	
	
	//with seconds
	echo $datetime->addSeconds(61). "<br>";                // 2012-02-03 00:01:01
	echo $datetime->addSecond(). "<br>";                   // 2012-02-03 00:01:02
	echo $datetime->subSecond(). "<br>";                   // 2012-02-03 00:01:01
	echo $datetime->subSeconds(61). "<br>";                // 2012-02-03 00:00:00
	
	
	
	//creating date time with seter string
	
	$date = Carbon::now();
	echo "<br>";
	$dateAndTime = $date -> year(1987) -> month(9) -> day(28) -> hour(12) ->  minute(30) -> second(30)-> toDateTimeString();
	echo "date-time string" . $dateAndTime . "<br>";
	$dateAndTimeSeperetly = $date->setDate(1975, 5, 21)->setTime(22, 32, 5)->toDateTimeString();
	echo $dateAndTimeSeperetly . "<br>";
	
	
	
	//Relative time, finding difference
	//in days
	$now = Carbon::now();
	$incriser = Carbon::now();
	$later = $incriser -> addMonths(3);
	
	$differenceDays = $now -> diffInDays($later);
	echo $differenceDays . "<br>";
	
	
	//in hours
	$laterHour = $incriser -> addHours(11);
	
	echo "<br>";
	echo $now -> diffInHours($laterHour); 
	
	echo "<br>";
	$laterYear = $incriser -> addYears(3);
	echo $now -> diffInYears($laterYear);
	
});

Route::get('/insert-traveler', function() {
	
	
	
	$data1 = [
			
		'email' => 'user@user.com',
		'name' => 'Marko',
		'lname' => 'Marulic',
		'address' => 'Zgubidanska 1',
		'city' => 'city 1',
		'creditcard' => '111-111-111-1',
		'favorits' => 'Argentina. Brazil',	
	];
	
	$data2 = [
				
			'email' => 'ivan@ivic.com',
			'name' => 'Ivan',
			'lname' => 'Ivic',
			'address' => 'Zgubidanska 2',
			'city' => 'city 2',
			'creditcard' => '111-111-111-2',
			'favorits' => 'Argentina. Brazil, Peru',
	];
	
	
	$data3 = [
				
			'email' => 'ivo@andric.com',
			'name' => 'Ivo',
			'lname' => 'Andric',
			'address' => 'Zgubidanska 3',
			'city' => 'city 3',
			'creditcard' => '111-111-111-3',
			'favorits' => 'Argentina. Brazil, sad',
	];
	
	if(DB::table('traveler') -> insert(array($data1, $data2, $data3))) {
	
		echo "succesfully inserted in db!";
	}
});


Route::get('/query', function(Request $request) {
	$email = 'luka@paljetak.com';
	
	
	//select with DB class (fluent)
	$travelers = DB::table('traveler') -> select('creditcard') -> where('email', '=', $email) -> get();
	//var_dump($travelers);
	
	foreach($travelers as $traveler) {
		echo $traveler -> creditcard;
	}
	echo "<br>";
	//select with Traveler class(eloquent)
	$travelers = Traveler::select('creditcard') -> where('email', '=', $email) -> take(1) -> get();
	//var_dump($travelers);
	foreach($travelers as $traveler) {
		echo $traveler -> creditcard;
	}
	echo "<br>";
	$traveler = Traveler::findOrFail(1);
	//var_dump($traveler);
	echo $traveler -> email;
	
	echo "<br>";
	$traveler = Traveler::where('email', '=', $email) -> firstOrFail();
	//var_dump($traveler);
	$favorits = unserialize($traveler -> favorits);
	foreach($favorits as $favorit) {
		echo $favorit;
	}
	
	
	$traveler = DB::table('traveler') -> select('creditcard') -> where('email', '=', 'janko@matko') -> get();
	
	$iv = session('iv');
	var_dump($iv);
	$key = "key";
	$method = "AES-128-CBC";
	foreach($traveler as $trave) {
		$decryptCard = openssl_decrypt($trave -> creditcard, $method, $key, 0, $iv);
		var_dump($decryptCard);
	}
	
	$data = $request -> session() -> all();
	var_dump($data);
	
	
	//join 
	
	
});



	//booking
	Route::get('/traveler-booking', 'TravelerAppController@getBookingForm');
	Route::post('/traveler-booking', 'TravelerAppController@saveBooking');
	
	//adding new traveler
	Route::get('/new-traveler', 'TravelerAppController@getNewTravelerForm');
	Route::post('/new-traveler', 'TravelerAppController@addNewTraveler');
	
	//showing traveler details
	Route::get('/traveler-details', 'TravelerAppController@getTravelerListingForm');
	Route::post('/traveler-details', 'TravelerAppController@listTravelerDetails');
	
	//encription method listing
	Route::get('/crypt', function() {
		$methods = openssl_get_cipher_methods();
		var_dump($methods);
	});



Route::get('/local', function() {
	$locale = App::getLocale();
	echo $locale;
	$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
	
	echo $language;
});



Route::get('/utenti-acl', function() {
	
		$travelers = App\Traveler::all();
		$destinations = App\Destination::all();
	
		//var_dump($users);
		echo "<br>";
		//var_dump($acls);
	
	
	
	
		foreach($travelers as $traveler) {
			foreach($destinations as $dest){
				if(!$traveler -> destination -> contains($dest)) {
					$traveler -> destination() -> attach($dest -> id);
				}
			}
	
		}
		return "acls added!";
});

Route::get('/session', function(Request $request) {
	
	//$request -> session() -> put('key', 'This is value');
	$request -> session() -> put('key2', 'This is soem other value');
	
	//$data = $request -> session() -> all();
	
	/* foreach($data as $key => $value) {
		echo $key . " =>" . $value;
	} */
	
	$request -> session() -> flash('data', 'This is some data');
	//return view('flushing') -> with('status', 'This is status');
	return redirect() -> route('flushing') -> with('status', 'This is status');
	
});

Route::get('/session2', function() {
	return view('flushing');
}) -> name('flushing');



Route::post('/request-play', function(Request $request) {
	$input = $request -> only(['classname', 'teacher']);
	
	echo $request -> input('classname');
	echo $request -> input('teacher');
	
	if(in_array('year', $input)) {
		echo $input['year'];
	} else {
		echo "input dose not exist";
	}
	
	
	
});

Route::get('/upload', 'StudentController@getUpload');
Route::post('/upload', 'StudentController@uploadFile');
Route::get('/download/{id?}', 'StudentController@downloadFile');
Route::get('/delete/{id?}', 'StudentController@deleteFile');
Route::get('/showFile/{id?}', 'StudentController@showFile');

Route::get('/showStudents', 'StudentController@showStudents');

//send mail routes
Route::post('/mail/{id?}', 'StudentController@sendMail');


Route::get('/pick', function() {
	return view('daterange');
});




