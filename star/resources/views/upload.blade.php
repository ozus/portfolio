<html>
	<head>
		<title>Upload file</title>
	</head>
	<body>
		@if(count($errors) > 0)
			@foreach($errors -> all() as $error)
				{{$error}}
			@endforeach
		@endif
		{{session('message')}}
		<form action='/upload' method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			<input type="file" name="file" /><br>
			<input type="submit" value="submit" />
		</form><br>
	
		@foreach($files as $file)
			<a href="download/{{$file -> id}}">{{$file -> name}}</a>  <a href="delete/<?php echo $file -> id; ?>">Delete</a> 
																	<a href="showFile/<?php echo $file -> id; ?>" target="_blank">Show</a> 
																<!--	<a href="{{action('StudentController@showFile', ['id' => $file -> id])}}">Show</a> -->  <br>
		@endforeach
		<br>
		@foreach($files as $file)
			@if(in_array($file -> extention, $ext))
				<img src="{{URL::to('/')}}/images/{{$file -> name}}{{$file -> extention}}" />
			@endif
		@endforeach
	</body>
</html>