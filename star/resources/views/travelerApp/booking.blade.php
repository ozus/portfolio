<html>
	<head>
		<title>Booking</title>
	</head>
	<body>
		@if(@count($errors) > 0)
			<ul>
				@foreach($errors -> all() as $error)
					<li>
						{{$error}}
					</li>
				@endforeach
			</ul>	
		@endif
		@if(session('message'))
			{{session('message')}}
		@endif
		@if(isset($successMessage))
			{{$successMessage}}
		@endif
		<?php 
			$travelers = $request -> session() -> get('travelers');
			$destinations = $request -> session() -> get('destinations');
		?>
		<form action="/traveler-booking" method="post">
		<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			@lang('lang.book.book')
			<select name="travelers[]" multiple>
				@foreach($travelers as $traveler)
					<option value="<?php echo $traveler -> id ?>">{{$traveler -> name}}  {{$traveler -> lname}}</option>
				@endforeach		
			</select>
			For destination
			<select name="destinations[]" multiple>
				@foreach($destinations as $destination)
					<option value="<?php echo $destination -> id ?>">{{$destination -> countrey}} - {{$destination -> city}}</option>
				@endforeach
			</select><br>
			<input type="submit" value="Book Travel" />
		</form>
	</body>
</html>