<html>
	<head>
		<title>List Traveler details</title>
	</head>
	<body>
		@if(@count($errors) > 0)
			<ul>
				@foreach($errors -> all() as $error)
					<li>
						{{$error}}
					</li>
				@endforeach
			</ul>	
		@endif
		@if(session('message'))
			{{session('message')}}
		@endif
		@if(isset($successMessage))
			{{$successMessage}}
		@endif
		<form action="/traveler-booking" method="post">
		<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			Travelers Email: <input type="email" name="email" /><br>
			<input type="submit" value="Book Travel" />
		</form>
	</body>
</html>