<html>
	<head>
		<title>Booking</title>
	</head>
	<body>
		@if(@count($errors) > 0)
			<ul>
				@foreach($errors -> all() as $error)
					<li>
						{{$error}}
					</li>
				@endforeach
			</ul>	
		@endif
		@if(session('errorMessage'))
			{{session('errorMessage')}}
		@endif
		@if(isset($successMessage))
			{{$successMessage}}
		@endif
		<?php 
		
		$contrys = $request -> session() -> get('contrys');
		
		?>
		<h2>New Traveler</h2><br>
		<form action="/new-traveler" method="post">
		<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			Email Address:<input type="email" name="email" /><br>
			Name: <input type="text" name="name" /><br>
			Last Name: <input type="text" name="lname" /><br>
			Address: <input type="text" name="address" /><br>
			City: <input type="text" name="city" /><br>
			Credit card number: <input type="text" name="creditcard" /><br>
			Choose favorit destiantions: 
			<select name="favorits[]" multiple>
				@foreach($contrys as $contry)
					<option value="<?php echo $contry -> country_name ?>">{{$contry -> contry_name}}</option>
				@endforeach
			</select><br>
			<input type="submit" value="Add new traveler in database" />
		</form>
	</body>
</html>