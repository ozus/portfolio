@extends('layouts.emails')

@section('title')	
	This is email
@endsection
@section('content')	
	Data:<br> 
	<p>{{$student -> name}}</p>
	<p>{{$student -> lname}}</p>
	<p>{{$student -> email}}</p>
	<p>{{ session('msg') }}</p>
@endsection
@section('footer')
	<a href="https://laravel.com/">Laravel</a>
@endsection
