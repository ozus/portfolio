<!DOCTYPE html>
<html>
	<head>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<title>Email from Laravel</title>
		<style>
			.body {
				display: "block";
				border: 1px solid black;
				background-color: #00FFFF;
			}
			.title {
				display: block;
				border: 2px solid red;
				background-color: #FAEBD7;
			}
			.wraper {
				display: block;
				background-color: #F0FFFF;
				border: 2px solid red;
				text-align: center;
			}
			.footer {
				display: "block";
				border: 1px solid black;
				background-color: #7FFF00;
			}
		</style>
	</head>
	<body class="body">
		
						<div class="title">	
							@yield('title')
						</div>
						<div class="wraper">
							@yield('content')
						</div>
			
			
					<div class="footer">
						@yield('footer')
					</div>
			
		
		
		
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js" type="text/javascript"></script>
	</body>
</html>