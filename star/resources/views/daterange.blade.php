<html>
<head>
	<title>Daterangepick</title>
{!! Html::style('css/AdminLTE.css') !!}	
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
{!! Html::style('css/bootstrap/bootstrap.min.css') !!}
{!! Html::style('css/datepicker/datepicker3.css') !!}
<link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

</head>
<body>

 

 
 <form action="smtn" method="post">
 	
 
	              
	              
	              
	              
	             <div class=container>
	             <div class = row >
	              <div class="col-md-6">
	              <div class="box">
	              
	              
	              	<div class="form-group">
	                <label>Date1:</label>
	
	                <input type="text" name="daterange" />
	              </div>
	              
	              
	              
	                <div class="form-group">
		                <label>Date range button:</label>
		
		                <div class="input-group">
		                  <button type="button" class="btn btn-default pull-right" id="daterange-btn">
		                    
		                  </button>
		                </div>
		              </div>
	              
	              
		        <div class="box-header with-border">
		          <h3 class="box-title">@lang('viahub.edit.boxtitle4')</h3>
		
		          <div class="box-tools pull-right">
		            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip">
		              <i class="fa fa-minus"></i></button>
		            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip">
		              <i class="fa fa-times"></i></button>
		          </div>
		        </div>
		        <div class="box-body">
		        
		        
		        	<div class="form-group">
		                <label>Date:</label>
		
		                <div class="input-group date">
		                  <div class="input-group-addon">
		                    <i class="fa fa-calendar"></i>
		                  </div>
		                  <input type="text" class="form-control pull-right" id="datepicker">
		                </div>
		                <!-- /.input group -->
		              </div>
		        
		        
		        
		        	<div class="form-group">
						<label>@lang('viahub.edit.address')</label>
						<input class="form-control" type="text" name="address"  />
					</div>
					<div class="form-group">
						<label>@lang('viahub.edit.number')</label>
						<input class="form-control" type="text" name="number"  />
					</div>
					<div class="form-group">
						<label>@lang('viahub.edit.city')</label>
						<input class="form-control" type="text" name="city" />
					</div>
					<div class="form-group">
						<label>@lang('viahub.edit.piva')</label>
						<input class="form-control" type="text" name="piva" />
					</div><div class="form-group">
						<label>@lang('viahub.edit.province')</label>
						<input class="form-control" type="text" name="province"  />
					</div>
					<div class="form-group">
						<label>@lang('viahub.edit.ragso')</label>
						<input class="form-control" type="text" name="ragso"/>
					</div>
					<div class="form-group">
						<label>@lang('viahub.edit.state')</label>
						<input class="form-control" type="text" name="state"  />
					</div>
		        </div>
		        <!-- /.box-body -->
		      </div>
	          </div>
	          </div>
	          </div>    
	              
	              
	              
	              
	              
	              <div class="form-group">
                        <input id="exportBtn" type="submit" class="btn btn-primary btn-lg" value="@lang('viahub.report.genreport')">
                    </div>
        
 </form>
 <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

 {!! Html::script('js/AdminLTE/app.js') !!}
  {!! Html::script('js/bootstrap/bootstrap.min.js') !!} 
  
 {!! Html::script('js/moment.js') !!}
 {!! Html::script('js/datepicker/bootstrap-datepicker.js') !!}
 <script>
//Date picker
    $('#datepicker').datepicker({
    autoclose: true
 });
</script>    
<script>
 $('input[name="daterange"]').daterangepicker(
		 {
		     locale: {
		       format: 'YYYY-MM-DD'
		     },
		     startDate: '2013-01-01',
		     endDate: '2013-12-31'
		 }, 
		 function(start, end, label) {
		     alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
		 });
 </script>
<script>
 $(function () {
$(".datetimeinput").daterangepicker({

"singleDatePicker": true,
"showDropdowns": true,
"timePicker": true,
"autoApply": true,

locale: {
format: "MM/DD/YYYY h:mm A"
}
});
});
</script>
<!-- Include Date Range Picker -->
<script>
$('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );
</script>

</body>
</html>