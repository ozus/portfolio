<html>
	<head>
		<title>New Student</title>
	</head>
	<body>
		@if(count($errors) > 0)
			@foreach($errors -> all() as $error)
				{{$error}}
			@endforeach
		@endif
		@if($_SERVER['REQUEST_METHOD'] == 'POST')
			@if($errorMessage)
				{{$errorMessage}}
			@endif
			@if($message)
				{{$message}}
			@endif
		@endif
		@if (session('message'))
    <div class="alert alert-success">
        {{ $message }}
    </div>
@endif
		<?php
			$students = $request -> session() -> get('students');
			$classes= $request -> session() -> get('classes');
			//$message = $request -> session() -> get('message');
			
			if($errorMessage) {
				echo $errorMessage;
			}
		?>
		<h2>Inlisting classe for students</h2><br>
		<form action="/student/multiple-insert" method="post" >
			<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			Select students:
			<select name ="students[]" multiple>
				<option value="bruce@lee.com">Bruce Lee</option>
				<option value="chuck@noris.com">Chuck Norris</option>
				<option value="albert@einstein.com">Albert Einstein</option>
			</select>
			Select class to inlist for students selected: 
			<select name ="classes[]" multiple>
				<option value="Mehanics">Mehanics</option>
				<option value="Optics">Optics</option>
				<option value="Theromdynamics">Theromdynamics</option>
				<option value="Electorodynamics">Electorodynamics</option>
				<option value="Eletronics">Eletronics</option>
				<option value="Quantum mechanics">Quantum mechanics</option>
			</select>
			<input type="submit" value="Inlist class" />
		</form>
		
		
		
		<form action="/student/multiple-insert" method="post" >
			<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			Select students:
			<select name ="students[]" multiple>
				@foreach($students as $student)
				<option value="<?php echo $student -> email; ?> ">{{$student -> name}} {{$student -> lname}}</option>
				@endforeach
			</select>
			Select class to inlist for students selected: 
			<select name ="classes[]" multiple>
				@foreach($classes as $class)
				<option value="<?php echo $class -> classname; ?>">{{$class -> classname}}</option>
				@endforeach
			</select>
			<input type="submit" value="Inlist class" />
		</form>
		
		<h2>Classes already inlisted:</h2>
		
		<?php 
		
		 foreach($students as $student) {
		 	echo "<br>";
			echo $student -> name . " " . $student -> lname . "<br>";
				foreach($student -> collageClass as $stu) {
				echo $stu -> classname . "<br>" ;
				}
		} 
		
		
		
		
		?>
		
	
		
	</body>
</html>