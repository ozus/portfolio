<html>
	<head>
		<title>New Student</title>
	</head>
	<body>
		@if(count($errors) > 0)
			@foreach($errors -> all() as $error)
				{{$error}}
			@endforeach
		@endif
		@if($_SERVER['REQUEST_METHOD'] == 'POST')
			@if($errorMessage)
				{{$errorMessage}}
			@endif
			@if($message)
				{{$message}}
			@endif
		@endif
		<h2>Inser new student details</h2><br>
		<form action="/student/insert" method="post" >
			<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			Student name:<input type="text" name="name" /><br>
			Student's last name:<input type="text" name="lname" /><br>
			Email:<input type="text" name="email" /><br>
			Password:<input type="text" name="password" /><br>
			Confirm Password:<input type="text" name="password_confirmation" /><br>
			<input type="submit" value="Inser Student" />
		</form>
	
	</body>
</html>