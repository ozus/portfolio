<html>
	<head>
		<title>New Student</title>
	</head>
	<body>
		@if(count($errors) > 0)
			@foreach($errors -> all() as $error)
				{{$error}}
			@endforeach
		@endif
		@if($_SERVER['REQUEST_METHOD'] == 'POST')
			@if($errorMessage)
				{{$errorMessage}}
			@endif
			@if($message)
				{{$message}}
			@endif
		@endif
		<h2>Inser new student details</h2><br>
		<form action="/request-play" method="post" >
			<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			Class Name:<input type="text" name="classname" /><br>
			Teachers Name:<input type="text" name="teacher" /><br>
			Year:<input type="number" name="year" /><br>
			Semester:<input type="number" name="semester" /><br>
			Class Code:<input type="text" name="classcode" /><br>
			<input type="submit" value="Inser Class" />
		</form>
	
	</body>
</html>

<!-- /student/insert-class -->