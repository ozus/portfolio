<html>
	<head>
		<title>New Student</title>
	</head>
	<body>
		@if(count($errors) > 0)
			@foreach($errors -> all() as $error)
				{{$error}}
			@endforeach
		@endif
		@if($_SERVER['REQUEST_METHOD'] == 'POST')
			@if($errorMessage)
				{{$errorMessage}}
			@endif
			@if($message)
				{{$message}}
			@endif
		@endif
		<h2>Update Classes</h2><br>
		<?php 
			$classes = $request -> session() -> get('classes');
		?>
		
		<form action="/student/update-class" method="post" >
			<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			Select Class:
			<select name ="classname_old">
				@foreach($classes as $class)
				<option value="<?php echo $class -> classname; ?> ">{{$class -> classname}}</option>
				@endforeach
			</select><br>
			Pleas input new values for class deails you want to change (you dont have to change all values, ones you dont want to change leav empty)
			<br>
			Class Name:<input type="text" name="classname" value="{!! old('classname') !!}" /><br>
			Teaher:<input type="text" name="teacher" value="{!! old('teacher') !!}" /><br>
			Year:<input type="number" name="year" value="{!! old('year') !!}" /><br>
			Semester:<input type="number" name="semester" value="{!! old('semester') !!}" /><br>
			<input type="submit" value="Update Class" />
		</form>
		
	</body>
</html>