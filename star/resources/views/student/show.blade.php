<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
		{{ Html::style('css/bootstrap/bootstrap.min.css') }}
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		
		<style>
		/* The Modal (background) */
			.modal {
			    display: none; /* Hidden by default */
			    position: fixed; /* Stay in place */
			    z-index: 1; /* Sit on top */
			    padding-top: 100px; /* Location of the box */
			    left: 0;
			    top: 0;
			    width: 100%; /* Full width */
			    height: 100%; /* Full height */
			    overflow: auto; /* Enable scroll if needed */
			    background-color: rgb(0,0,0); /* Fallback color */
			    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
			}
			
			/* Modal Content */
			.modal-content {
			    background-color: #fefefe;
			    margin: auto;
			    padding: 20px;
			    border: 1px solid #888;
			    width: 80%;
			}
			
			/* The Close Button */
			.close {
			    color: #aaaaaa;
			    float: right;
			    font-size: 28px;
			    font-weight: bold;
			}
			
			.close:hover,
			.close:focus {
			    color: #000;
			    text-decoration: none;
			    cursor: pointer;
			}
		</style>
	</head>
	<body>
	
	<div class="container">
		@foreach($students as $student)
		<div id="modl{{$student -> id}}" class="row modal">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body modal-content">
						<span id="close{{$student -> id}}" class="close">X</span>
						<label><h2>Send Email to {{$student -> name}}  {{$student -> lname}}</h2></label>
						<form id="myForm{{$student -> id}}" action ="/mail/<?php echo $student -> id; ?>" name="form" method="post">
							<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
							<div class="form-group">
								<label>Email Address:</label>
								<input class="form-control" type="email" name="mail" />
							</div>
							<div class="form-group">
								<label>Message:</label>
								 <textarea class="form-control" rows="4" cols="50" name="message">

								</textarea> 
							</div>
							<div class="form-group">
								<input class="btn btn-primary" type="submit" value="Send" />
							</div>
							
						</form>	
					</div>
				</div>
			</div>
		</div>
		@endforeach
		
		<div class="row">
			<div class="col-md-8">
				<table  class="table table-hover table-bordered">
					<thead>
						<tr>
						<th>Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Send Mail</th>
						</tr>
					</thead>
					<tbody>				
						@foreach($students as $student)
							
							<tr>
								<td>
									{{$student -> name}}
								</td>
								<td>
									{{$student -> lname}}
								</td>
								<td>
									{{$student -> email}}
								</td>
								<td>
									<button type="button"  class="btn btn-default" onclick="passId('<?php echo $student -> id; ?>')">Send Email</button>
									<button type="button" id="<?php echo $student -> name; ?>" class="btn btn-default">S Email</button> 
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<script>
	@foreach($students as $student)
		var modl<?php echo json_encode($student -> id); ?> = document.getElementById('modl<?php echo $student -> id; ?>');
	
		var close<?php echo json_encode($student -> id); ?> = document.getElementById('close<?php echo $student -> id; ?>');
	
		var btn<?php echo json_encode($student -> id); ?> = document.getElementById('<?php echo $student -> name; ?>');
	
		btn<?php echo json_encode($student -> id); ?>.onclick  = function() {
			modl<?php echo json_encode($student -> id); ?>.style.display = 'block';
			//alert('Working!');
		}
	
		close<?php echo json_encode($student -> id); ?>.onclick = function() {
			modl<?php echo json_encode($student -> id); ?>.style.display = 'none';
		}
	
		window.onclick = function(event) {
		    if (event.target == modl<?php echo json_encode($student -> id); ?>) {
		        modl<?php echo json_encode($student -> id); ?>.style.display = "none";
		    }
		}
	@endforeach	

	
		function red(id) {
				
			window.location = 'mail/' + id;
		}

		
		function passId(id) {
			alert(<?php echo json_encode("student"); ?>)
			//var frm = document.getElementById('myForm');
			//frm.action = '/mail/' +id;
		}
	</script>
	
		{{ Html::script('js/bootstrap/bootstrap.min.js') }}
	</body>
</html>