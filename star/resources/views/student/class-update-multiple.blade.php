<html>
	<head>
		<title>Classes Update</title>
	</head>
	<body>
		@if(count($errors) > 0)
			@foreach($errors -> all() as $error)
				{{$error}}
			@endforeach
		@endif
		@if($_SERVER['REQUEST_METHOD'] == 'POST')
			@if($errorMessage)
				{{$errorMessage}}
			@endif
			@if($message)
				{{$message}}
			@endif
		@endif
		<h2>Update Casses</h2><br>
		<?php 
			$classes = $request -> session() -> get('classes');
		?>
		
		<form action="/student/update-multiple-class" method="post" >
			<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			Select students:
			<select name ="classes[]" multiple>
				@foreach($classes as $class)
				<option value="<?php echo $class -> classname; ?> ">{{$class -> classname}}</option>
				@endforeach
			</select>
			Pleas input new values for classes deails you want to change (you dont have to change all values, ones you dont want to change leave empty)
			<br>
			Year:<input type="number" name="year" value="{!! old('year') !!}" /><br>
			Semester:<input type="number" name="semester" value="{!! old('semester') !!}" /><br>
			<input type="submit" value="Update Classes" />
		</form>
		