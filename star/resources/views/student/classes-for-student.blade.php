<html>
	<head>
		<title>New Student</title>
	</head>
	<body>
		@if(count($errors) > 0)
			@foreach($errors -> all() as $error)
				{{$error}}
			@endforeach
		@endif
		@if($_SERVER['REQUEST_METHOD'] == 'POST')
			@if($errorMessage)
				{{$errorMessage}}
			@endif
			@if($message)
				{{$message}}
			@endif
		@endif
		<h2>Inlisting classe for student</h2><br>
		<form action="/student/inlist-class" method="post" >
			<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			Student name:<input type="email" name="email" /><br>
			Select class to inlist: 
			<select name ="classes">
				<option value="Mehanics">Mehanics</option>
				<option value="Optics">Optics</option>
				<option value="Theromdynamics">Theromdynamics</option>
				<option value="Eletronics">Eletronics</option>
				<option value="Quantum mechanics">Quantum mechanics</option>
			</select>
			<input type="submit" value="Inlist class" />
		</form>
	
	</body>
</html>