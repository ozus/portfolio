<html>
	<head>
		<title>New Class fro student</title>
	</head>
	<body>
		@if(count($errors) > 0)
			@foreach($errors -> all() as $error)
				{{$error}}
			@endforeach
		@endif
		@if($_SERVER['REQUEST_METHOD'] == 'POST')
			@if($errorMessage)
				{{$errorMessage}}
			@endif
			@if($message)
				{{$message}}
			@endif
		@endif
		<h2>Add students to a class</h2><br>
		<form action="/student/inlist-studnet" method="post" >
			<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			Student name:<input type="text" name="classname" /><br>
			Select class to inlist: 
			<select name ="students">
				<option value="mario@maric.com">Mario Maric</option>
				<option value="iva@ivic.com">Iva Ivic</option>
				<option value="Marko@Marulic.com">Marko Marulic</option>
				<option value="dario@daric.com">Dario Daric</option>
				<option value="ivo@andric.com">Ivo Andric</option>
			</select>
			<input type="submit" value="Inlist student" />
		</form>
	
	</body>
</html>