<html>
	<head>
		<title>New class for student</title>
	</head>
	<body>
		@if(count($errors) > 0)
			@foreach($errors -> all() as $error)
				{{$error}}
			@endforeach
		@endif
		@if($_SERVER['REQUEST_METHOD'] == 'POST')
			@if($errorMessage)
				{{$errorMessage}}
			@endif
			@if($message)
				{{$message}}
			@endif
		@endif
		<h2>Create class for student</h2><br>
		<form action="/student/create-class-for-student" method="post" >
			<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			Student name:<input type="email" name="email" /><br>
			Class Name: <input type="text" name="classname" /><br>
			Teacher: <input type="text" name="teacher" /><br>
			Year: <input type="number" name="year" /><br>
			Semester:<input type="number" name="semester" /><br>
			Class Code:<input type="text" name="classcode" /><br>
 			<input type="submit" value="Inlist class" />
		</form>
	
	</body>
</html>